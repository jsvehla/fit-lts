#ifndef FACADE_GTEST__L3U930338938KSJAHFKLKDSHF89348KHFD
#define FACADE_GTEST__L3U930338938KSJAHFKLKDSHF89348KHFD

#include "..\src\LTS\LTSAlgorithm.h"
#include "..\src\LTS\LTSAlgorithmVA.h"
#include "..\src\LTS\LTSAlgorithmBSA.h"
#include "..\src\LTS\LTSAlgorithmBSAM.h"
#include "..\src\LTS\BSACacheHashMap.h"

namespace gtestfacade {

extern const unsigned char CACHE_NULL;
extern const unsigned char CACHE_HASHMAP;

LTSAlgorithmBSA* createBSA(
	int hParam, arma::mat& explVars,
	arma::mat& respVar,
	BSACache* cache,
	bool intercept = false );

LTSAlgorithmBSAM* createBSAM(
	int hParamL,
	int hParamH,
	arma::mat& explVars,
	arma::mat& respVar,
	BSACache* cache,
	bool intercept = false );

LTSAlgorithmVA* createVA(
	int kParam,
	int hParam,
	int cycles,
	arma::mat& explVars,
	arma::mat& respVar );

BSACache* createCache( char flags );

void deleteCache( BSACache* cache );

void deleteLTSAlgorithm( LTSAlgorithm* ltsalg );
}

#endif //FACADE_GTEST__L3U930338938KSJAHFKLKDSHF89348KHFD