#include "FacadeGTest.h"


namespace gtestfacade {
const unsigned char CACHE_NULL		= 0x00;
const unsigned char CACHE_HASHMAP	= 0x01;


LTSAlgorithmBSA* createBSA(
	int hParam, arma::mat& explVars,
	arma::mat& respVar,
	BSACache* cache,
	bool intercept ) {

	return new LTSAlgorithmBSA( hParam, explVars, respVar, *cache, intercept );
}

LTSAlgorithmBSAM* createBSAM(
	int hParamL,
	int hParamH,
	arma::mat& explVars,
	arma::mat& respVar,
	BSACache* cache,
	bool intercept ) {

	return new LTSAlgorithmBSAM( hParamL, hParamH, explVars, respVar, *cache, intercept );
}

LTSAlgorithmVA* createVA(
	int kParam,
	int hParam,
	int cycles,
	arma::mat& explVars,
	arma::mat& respVar ) {

	return new LTSAlgorithmVA( kParam, hParam, cycles, explVars, respVar );
}

BSACache* createCache( char flags ) {
	BSACache* cache = NULL;

	if( flags == 0x01 ) {
		//cache = new BSACacheHashMap(2147487);
		cache  = new BSACacheNull();
	} else {
		cache  = new BSACacheNull();
	}

	return cache;
}

void deleteCache( BSACache* cache ) {
	delete cache;
}

void deleteLTSAlgorithm( LTSAlgorithm* ltsalg ) {
	delete ltsalg;
}

};