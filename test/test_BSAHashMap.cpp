#include "gtest/gtest.h"

#include "armadillo"

#include "FacadeGTest.h"


/* from test_VA */
void compareResult(const LTSResult& result, const arma::vec& w, const double sumR, const arma::vec& B, const double absErr);

void disableExceptionPrint();

/* BSA TEST SUITE */
TEST( LTSAlgorithmBSAHashMap, D1 ) {
	arma::mat X = arma::mat( "1 217 67 260 91; 1 141 52 190 66; 1 152 58 203 68; 1 153 56 183 70; 1 180 66 170 77; 1 193 71 178 82; 1 162 65 160 74; 1 180 80 170 84; 1 205 77 188 83; 1 168 74 170 79; 1 232 65 220 72; 1 146 68 158 68; 1 173 51 243 56; 1 155 64 198 59; 1 212 66 220 77; 1 138 70 180 62; 1 147 54 150 75; 1 197 76 228 88; 1 165 59 188 70; 1 125 58 160 66; 1 161 52 190 69; 1 132 62 163 59; 1 257 64 313 96; 1 236 72 225 84; 1 149 57 173 68; 1 161 57 173 65; 1 198 59 220 62; 1 245 70 218 69; 1 141 63 193 60; 1 177 53 183 75; " );
	arma::mat Y = arma::mat( "481; 292; 338; 357; 396; 429; 345; 469; 425; 358; 393; 346; 279; 311; 401; 267; 404; 442; 368; 295; 391; 264; 487; 481; 374; 309; 367; 469; 252; 338;" );
	arma::vec w = arma::vec( "1 0 1 1 1 1 0 0 1 1 1 0 1 0 1 1 1 1 1 1 0 1 0 0 0 1 0 0 1 0" );
	arma::vec OLS = arma::vec( "-11.8001 0.7410 -2.6552 -0.2455 6.3733" );

	disableExceptionPrint();
	BSACache* cache = gtestfacade::createCache(gtestfacade::CACHE_HASHMAP);
	LTSAlgorithmBSA* a = gtestfacade::createBSA(18,X, Y, cache, true);
	
	LTSResult result;
	a->solve(result);

	compareResult(result, w, 1721.2, OLS, 0.0001);

	gtestfacade::deleteCache(cache);
	gtestfacade::deleteLTSAlgorithm(a);
}

TEST(  LTSAlgorithmBSAHashMap, D2 ) {
	arma::mat X = arma::mat( "1 80 27 89; 1 80 27 88; 1 75 25 90; 1 62 24 87; 1 62 22 87; 1 62 23 87; 1 62 24 93; 1 62 24 93; 1 58 23 87; 1 58 18 80; 1 58 18 89; 1 58 17 88; 1 58 18 82; 1 58 19 93; 1 50 18 89; 1 50 18 86; 1 50 19 72; 1 50 19 79; 1 50 20 80; 1 56 20 82; 1 70 20 91;" );
	arma::mat Y = arma::mat( "42; 37; 37; 28; 18; 18; 19; 20; 15; 14; 14; 13; 11; 12; 8; 7; 8; 8; 9; 15; 15;" );
	arma::vec w = arma::vec( "0 0 0 0 1 1 1 0 1 1 1 1 0 0 1 1 1 1 1 0 0" );
	arma::vec OLS = arma::vec("-35.2095 0.746057 0.337795 -0.0054919");

	disableExceptionPrint();
	BSACache* cache = gtestfacade::createCache(gtestfacade::CACHE_HASHMAP);
	LTSAlgorithmBSA* a = gtestfacade::createBSA(12, X, Y, cache, true);
	
	LTSResult result;
	a->solve(result);

	compareResult(result, w, 1.6371, OLS, 0.0001);

	gtestfacade::deleteCache(cache);
	gtestfacade::deleteLTSAlgorithm(a);
}

TEST( LTSAlgorithmBSAHashMap, D3 ) {
	arma::mat X = arma::mat( "1 13.3 13.9 31 697; 1 13.3 14.1 30 697; 1 13.4 15.2 32 700; 1 12.7 13.8 31 669; 1 14.4 13.6 31 631; 1 14.4 13.8 30 638; 1 14.5 13.9 32 643; 1 14.2 13.7 31 629; 1 12.2 14.8 36 724; 1 12.2 15.3 35 739; 1 12.2 14.9 36 722; 1 12.0 15.2 37 743; 1 12.9 15.4 36 723; 1 12.7 16.1 35 649; 1 12.9 15.1 36 721; 1 12.7 15.9 37 696;" );
	arma::mat Y = arma::mat( "84.4; 84.1; 88.4; 84.2; 89.8; 84.0; 83.7; 84.1; 90.5; 90.1; 89.4; 90.2; 93.8; 93.0; 93.3; 93.1" );
	arma::vec w = arma::vec( "1 1 1 1 0 1 0 1 0 1 1 1 0 1 0 1" );
	arma::vec OLS = arma::vec("35.1134 -0.0275 2.9489 0.4774 -0.0091");

	disableExceptionPrint();
	BSACache* cache = gtestfacade::createCache(gtestfacade::CACHE_HASHMAP);
	LTSAlgorithmBSA* a = gtestfacade::createBSA( 11, X, Y, cache, true);
	
	LTSResult result;
	a->solve(result);

	compareResult(result, w, 0.2707, OLS, 0.0001);

	gtestfacade::deleteCache(cache);
	gtestfacade::deleteLTSAlgorithm(a);
}

TEST( LTSAlgorithmBSAHashMap, D4 ) {
	arma::mat X = arma::mat( "1 6.3 1.7 8176 4500; 1 6 1.9 6699 3120; 1 5.9 1.5 9663 6300; 1 3 1.2 12837 9800; 1 5 1.8 10205 4900; 1 6.3 2 14890 6500; 1 5.6 1.6 13836 8920; 1 3.6 1.2 11628 14500; 1 2 1.4 15225 14800; 1 2.9 2.3 18691 10900; 1 2.2 1.9 19350 16000; 1 3.9 2.6 20638 16000; 1 4.5 2 12843 7800; 1 4.3 9.7 13384 17900; 1 4 2.9 13307 10500; 1 3.2 4.3 29855 24500; 1 4.3 4.3 29277 30000; 1 2.4 2.6 24651 24500; 1 2.8 3.7 28539 34000; 1 3.9 3.3 8085 8160; 1 2.8 3.9 30328 35800; 1 1.6 4.1 46172 37000; 1 3.4 2.5 17836 19600;" );
	arma::mat Y = arma::mat( "2.76; 4.76; 8.75; 7.78; 6.18; 9.50; 5.14; 4.76; 16.70; 27.68; 26.64; 13.71; 12.31; 15.73; 13.59; 51.90; 20.78; 29.82; 32.78; 10.12; 27.84; 107.10; 11.19;" );
	arma::vec w = arma::vec( "1 0 0 0 1 1 1 1 1 1 1 0 1 1 1 0 1 0 0 1 0 0 1" );
	arma::vec OLS = arma::vec("15.2678 -4.4961 1.8755 0.0018 -0.0012");

	disableExceptionPrint();
	BSACache* cache = gtestfacade::createCache(gtestfacade::CACHE_HASHMAP);
	LTSAlgorithmBSA* a = gtestfacade::createBSA( 14, X, Y, cache, true);
	
	LTSResult result;
	a->solve(result);

	compareResult(result, w, 36.0336, OLS, 0.0001);

	gtestfacade::deleteCache(cache);
	gtestfacade::deleteLTSAlgorithm(a);
}

TEST( LTSAlgorithmBSAHashMap, D5 ) {
	arma::mat X = arma::mat( "1.35; 465; 36.33; 27.66; 1.04; 11700; 2547; 187.1; 521; 10; 3.3; 529; 207; 62; 6654; 9400; 6.8; 35; 0.12; 0.023; 2.5; 55.5; 100; 52.16; 87000; 0.28; 0.122; 192;" );
	arma::mat Y = arma::mat( "8.1; 423; 119.5; 115; 5.5; 50; 4603; 419; 655; 115; 25.6; 680; 406; 1320; 5712; 70; 179; 56; 1; 0.4; 12.1; 175; 157; 440; 154.5; 1.9; 3; 180;" );
	arma::vec w = arma::vec( "1 0 1 1 1 0 0 1 0 0 1 0 1 0 0 0 0 1 1 1 1 1 1 0 0 1 1 0" );
	arma::vec OLS = arma::vec("2.0948");

	disableExceptionPrint();
	BSACache* cache = gtestfacade::createCache(gtestfacade::CACHE_HASHMAP);
	LTSAlgorithmBSA* a = gtestfacade::createBSA( 15,  X, Y, cache, false);
	
	LTSResult result;
	a->solve(result);

	compareResult(result, w, 13583.6, OLS, 0.0001);

	gtestfacade::deleteCache(cache);
	gtestfacade::deleteLTSAlgorithm(a);
}

TEST( LTSAlgorithmBSAHashMap, D6 ) {
	arma::mat X = arma::mat( "1 8.2 4 23.005; 1 7.6 5 23.873; 1 4.6 0 26.417; 1 4.3 1 24.868; 1 5.9 2 29.895; 1 5 3 24.2; 1 6.5 4 23.215; 1 8.3 5 21.862; 1 10.1 0 22.274; 1 13.2 1 23.83; 1 12.6 2 25.144; 1 10.4 3 22.43; 1 10.8 4 21.785; 1 13.1 5 22.38; 1 13.3 0 23.927; 1 10.4 1 33.443; 1 10.5 2 24.859; 1 7.7 3 22.686; 1 10 0 21.789; 1 12 1 22.041; 1 12.1 4 21.033; 1 13.6 5 21.005; 1 15 0 25.865; 1 13.5 1 26.29; 1 11.5 2 22.932; 1 12 3 21.313; 1 13 4 20.769; 1 14.1 5 21.393;" );
	arma::mat Y = arma::mat( "7.6; 7.7; 4.3; 5.9; 5; 6.5; 8.3; 8.2; 13.2; 12.6; 10.4; 10.8; 13.1; 12.3; 10.4; 10.5; 7.7; 9.5; 12; 12.6; 13.6; 14.1; 13.5; 11.5; 12; 13; 14.1; 15.1; " );
	arma::vec w = arma::vec( "0 1 1 1 0 1 1 0 0 0 0 1 0 1 1 0 1 1 1 1 1 1 0 0 0 1 1 0" );
	arma::vec OLS = arma::vec("36.65 0.3893 -0.1142 -1.3067");

	disableExceptionPrint();
	BSACache* cache = gtestfacade::createCache(gtestfacade::CACHE_HASHMAP);
	LTSAlgorithmBSA* a = gtestfacade::createBSA( 16, X, Y, cache, true);
	
	LTSResult result;
	a->solve(result);

	compareResult(result, w, 0.6980, OLS, 0.0001);

	gtestfacade::deleteCache(cache);
	gtestfacade::deleteLTSAlgorithm(a);
}

TEST( LTSAlgorithmBSAHashMap, D7 ) {
	arma::mat X = arma::mat( "1 508 3944 325; 1 564 4578 323; 1 322 4011 328; 1 846 5233 305; 1 871 4780 303; 1 774 5889 307; 1 856 5663 301; 1 889 5759 310; 1 715 4894 300; 1 753 5012 324; 1 649 4908 329; 1 830 5753 320; 1 738 5439 337; 1 659 4634 328; 1 664 4921 330; 1 572 4869 318; 1 701 4672 309; 1 443 4782 333; 1 446 4296 330; 1 615 4827 318; 1 661 5057 304; 1 722 5540 328; 1 766 5331 323; 1 631 4715 317; 1 390 3828 310; 1 450 4120 321; 1 476 3817 342; 1 603 4243 339; 1 805 4647 287; 1 523 3967 325; 1 588 3946 315; 1 584 3724 332; 1 445 3448 358; 1 500 3680 320; 1 661 3825 355; 1 680 4189 306; 1 797 4336 335; 1 534 4418 335; 1 541 4323 344; 1 605 4813 331; 1 785 5046 324; 1 698 3764 366; 1 796 4504 340; 1 804 4005 378; 1 809 5560 330; 1 726 4989 313; 1 671 4697 305; 1 909 5438 307; 1 831 5309 333; 1 484 5613 386;" );
	arma::mat Y = arma::mat( "235; 231; 270; 261; 300; 317; 387; 285; 300; 221; 264; 308; 379; 342; 378; 232; 231; 246; 230; 268; 337; 344; 330; 261; 214; 245; 233; 250; 243; 216; 212; 208; 215; 221; 244; 234; 269; 302; 268; 323; 304; 317; 332; 315; 291; 312; 316; 332; 311; 546;" );
	arma::vec w = arma::vec( "1 1 0 1 0 0 0 1 0 0 1 1 0 0 0 0 1 1 1 1 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0 1 0 0 0 0 0 1 0 0 0 1 0" );
	arma::vec OLS = arma::vec("-143.50 0.04 0.04 0.64");

	disableExceptionPrint();
	BSACache* cache = gtestfacade::createCache(gtestfacade::CACHE_HASHMAP);
	LTSAlgorithmBSA* a = gtestfacade::createBSA( 27, X, Y, cache, true);
	
	LTSResult result;
	a->solve(result);

	compareResult(result, w, 3414.45, OLS, 0.01);

	gtestfacade::deleteCache(cache);
	gtestfacade::deleteLTSAlgorithm(a);
}