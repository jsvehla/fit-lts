What is ArmaNpy?
----------------

ArmaNpy is a set of SWIG interface files which allows for generating
Python bindings to C++ code which uses the Armadillo matrix library.
From within Python any Armadillo matrices are represented as NumPy matrices.
This is possible due to the same memory layout used.
Copying of memory is avoided whenever possible.
It also supports boost:shared_ptr wrapped return values of Armadillo matrices.

Usage
-----

See the file example/example.i and test/test.i how to use ArmaNpy to generate bindings for a set of classes
defined in example/example.hpp and test/test.hpp.
See example/example.py and test/armanpy_tests.py for the Python side of usage (Note that armanpy_tests.py is jut unit tests).

How to compile and run the example and the tests?
-------------------------------------------------

1) Use CMake to generate your build system based on the provided CMakeLists file in the example or test dir.
2) Build the resulting project.
3) Run the Python script example.py or armanpy_tests.py

From the command line this can be done as follows for the example dir (adapt to your path settings and use '\' instedead of '/' on Windows). The same holds for the test dir.

cd /path/to/armanpy/example
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release -DARMADILLO_INCLUDE_DIR=/path/to/your/armadillo/include
cmake --build . --config Release
cd ..
python example.py --verbose

*Note* : -DCMAKE_BUILD_TYPE=Release is used bcs on Windows a typical Python distribution does not
come with the debug version of the Python libraries. Hence, the build would fail for the Debug
configuration.

Credits
-------

The idea of wrapping armadillo matrices py NumPy arrays was taken from Michael Forbes`
https://bitbucket.org/mforbes/swig_cpp_python/src/cd7cef820273?at=default.

Copyright
---------

Copyright (C) 2008-2012 Thomas Natschläger (thomas.natschlaeger@gmail.com)

License
-------

ArmaNpy is provided without any warranty of fitness
for any purpose. You can redistribute this file
and/or modify it under the terms of the GNU
Lesser General Public License (LGPL) as published
by the Free Software Foundation, either version 3
of the License or (at your option) any later version.
(see http://www.opensource.org/licenses for more info)

TODO
----

* allow for const arma::Mat< ... > * (should be the same as const arma::Mat< ... > &)
* allow for arma::Mat< ... > *( should be the same as arma::Mat< ... > &)
