cmake_minimum_required(VERSION 2.8.5 FATAL_ERROR)

project( FITLTS_CLI )

SET( CMAKE_VERBOSE_MAKEFILE ON )

file(GLOB FITLTS_SRC src/LTS/*)
file(GLOB CLI_SRC src/*)

### TCLAP
find_path( TCLAP_INCLUDE_DIR
		NAMES tclap
		PATHS ./lib/tclap
		PATH_SUFFIXES "include"
)
message( STATUS "TClap" )
message( STATUS "  IncDirs: ${TCLAP_INCLUDE_DIR}" )

### Armadillo
# SET( ARMA_PATH "./lib/armadillo-3.4.4")
SET( ARMA_PATH "/usr/include")
find_path( ARMADILLO_INCLUDE_DIR
		NAMES armadillo
		PATHS ${ARMA_PATH}
		PATH_SUFFIXES "include"
)
message( STATUS "Armadillo" )
message( STATUS "  IncDirs: ${ARMADILLO_INCLUDE_DIR}" )

### LAPACK
# SET( LAPACK_NAMES "lapack_win32_MT")
# SET( LAPACK_PATH "./lib/armadillo-3.4.4/examples/lib_win32")
SET( LAPACK_PATH /usr/lib)
SET( LAPACK_NAMES lapack)
FIND_LIBRARY(LAPACK_LIBRARY
  NAMES ${LAPACK_NAMES}
  PATHS ${LAPACK_PATH}
)
if(LAPACK_LIBRARY)
  message( STATUS "Lapack")
  message( STATUS "  Lapack link library is ${LAPACK_LIBRARY}")
else()
  message( STATUS "Lapack NOT FOUND" )
endif()

### BLAS
# SET( BLAS_PATH "./lib/armadillo-3.4.4/examples/lib_win32")
# SET( BLAS_NAMES "blas_win32_MT")
SET( BLAS_PATH /usr/lib)
SET( BLAS_NAMES blas)
FIND_LIBRARY(BLAS_LIBRARY
  NAMES ${BLAS_NAMES}
  PATHS ${BLAS_PATH}
)
if(BLAS_LIBRARY)
  message( STATUS "BLAS")
  message( STATUS "  BLAS link library is ${BLAS_LIBRARY}")
else()
  message( STATUS "BLAS NOT FOUND" )
endif()

### Boost
#SET(BOOST_ROOT "C:\\libs\\boost_1_53_0")
find_package( Boost 1.42.0 REQUIRED )
message( STATUS "Boost C++" )
message( STATUS "    IncDirs: ${Boost_INCLUDE_DIRS}" )


if( MSVC )
	# for multi-config build MSVC
	foreach( OUTPUTCONFIG ${CMAKE_CONFIGURATION_TYPES} )
		string( TOUPPER ${OUTPUTCONFIG} OUTPUTCONFIG )
		set( CMAKE_RUNTIME_OUTPUT_DIRECTORY_${OUTPUTCONFIG} "${PROJECT_BINARY_DIR}/bin" )
		set( CMAKE_LIBRARY_OUTPUT_DIRECTORY_${OUTPUTCONFIG} "${PROJECT_BINARY_DIR}/bin" )
		set( CMAKE_ARCHIVE_OUTPUT_DIRECTORY_${OUTPUTCONFIG} "${PROJECT_BINARY_DIR}/bin" )
	endforeach()
else()
	# For the generic no-config case (e.g. with mingw)
	set( CMAKE_RUNTIME_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/bin" )
	set( CMAKE_LIBRARY_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/bin" )
	set( CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/bin" )
	#SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O1") 
endif()

include_directories(
	${ARMADILLO_INCLUDE_DIR}
	${TCLAP_INCLUDE_DIR}
	${Boost_INCLUDE_DIRS}
	src/LTS/
)

add_library( FIT-LTS-Lib STATIC ${FITLTS_SRC})

add_executable ( FIT-LTS  ${CLI_SRC})
target_link_libraries(FIT-LTS FIT-LTS-Lib ${BLAS_LIBRARY} ${LAPACK_LIBRARY} )
