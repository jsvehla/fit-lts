#include <iostream>
#include <fstream>
#include <vector>

#include "tclap/CmdLine.h"
#include "armadillo"

#include "global_defines.h"
#include "LTS\LTSAlgorithm.h"
#include "LTS\LTSAlgorithmVA.h"
#include "LTS\LTSAlgorithmBSA.h"
#include "LTS\Combination.h"
#include "LTS\BSACache.h"
#include "LTS\BSACacheNull.h"
#include "LTS\BSACacheHashMap.h"

#define NO_ARMA_CERR

bool parseFile( const std::string& filename, bool intercept, bool hasID , arma::mat& data );

int main( int argc, char* argv[] ) {
	try {
		/* command line arguments */
		TCLAP::CmdLine cmd( "Command description message", ' ', VERSION );

		TCLAP::ValueArg<unsigned int> respPosArg(
			"r",
			"response",
			"Column number (starting from 1) of response variable. Set 0 for last column.",
			false,
			0,
			"unsigned int" );
		cmd.add( respPosArg );

		TCLAP::SwitchArg idSw( "i",
							   "noid",
							   "Input file doesn't include ID column.",
							   false );
		cmd.add( idSw );

		TCLAP::SwitchArg interceptSw(
			"p",
			"intercept",
			"Add intercept to the data matrix.",
			false );
		cmd.add( interceptSw );

		TCLAP::ValueArg<unsigned int> lowNotrimArg(
			"l",
			"lownottrimmed",
			"Number of lower boundary for not-trimmed measurements. Valid only for BSAM algorithm",
			false,
			-1,
			"unsigned int" );
		cmd.add( lowNotrimArg );

		TCLAP::ValueArg<unsigned int> notrimArg(
			"t",
			"nottrimmed",
			"Number of not-trimmed measurements.",
			true,
			-1,
			"unsigned int" );
		cmd.add( notrimArg );

		TCLAP::ValueArg<std::string> filenameArg(
			"f",
			"file",
			"Input datafile.",
			true,
			"",
			"string" );
		cmd.add( filenameArg );

		std::vector<std::string> allowed;
		allowed.push_back( "VA" );
		allowed.push_back( "BSA" );
		allowed.push_back( "BSAM" );
		TCLAP::ValuesConstraint<std::string> allowedVals( allowed );
		TCLAP::MultiArg<std::string> algorithmArg(
			"a",
			"algorithm",
			"Algorithm used to get LTS estimate.",
			true,
			&allowedVals );
		cmd.add( algorithmArg );

		TCLAP::SwitchArg cacheSw(
			"c",
			"cache",
			"Use cache to remember intermediate values. Valid only for BSA/M ",
			false );
		cmd.add( cacheSw );

		cmd.parse( argc, argv );

		// Filename check, file parsing
		arma::mat explVars;

		if( !parseFile( filenameArg.getValue(), interceptSw.getValue(), !idSw.getValue(), explVars ) ) {
			throw TCLAP::ArgException( "File cannot be openned.", "file" );
		}

		// col_num of resp var check and creating of respVar column
		unsigned int respPos = respPosArg.getValue();
		if( respPos == 0 ) {
			respPos = ( explVars.n_cols );
		}

		if( respPos > explVars.n_cols || respPos <= 0 ) {
			throw TCLAP::ArgException( "Invalid column number.", "response" );
		}

		arma::vec respVar = explVars.col( respPos - 1 );
		explVars.shed_col( respPos - 1);


		// hParam check
		if( notrimArg.getValue() <= 0 || notrimArg.getValue() > explVars.n_rows ) {
			throw TCLAP::ArgException( "Number of not-trimmed explanatory variables must be positive integer and must no be higher that number of rows.", "nottrimmed" );
		}

#ifdef _DEBUG
		respVar.t().raw_print();
		std::cout << std::endl;
		explVars.raw_print();
#endif

#ifdef NO_ARMA_CERR
		std::ostream nullstream( 0 );
		arma::set_stream_err2( nullstream );
		arma::set_stream_err1( nullstream );
#endif

		LTSResult result;

		// check for arguments and run all algorithms user wants
		std::vector<std::string> algVec = algorithmArg.getValue();

		if( std::find( algVec.begin(), algVec.end(), "VA" ) != algVec.end() ) {
			std::cout << "VA Algorithm START." << std::endl;

			LTSAlgorithmVA va = LTSAlgorithmVA(
									explVars.n_cols + 1,
									notrimArg.getValue(),
									1000,
									explVars,
									respVar
								);
			va.solve( result );

			std::cout << "VA Algorithm END. Result: " << std::endl;
			std::cout << "min w: ";
			result.getVector().t().raw_print();
			std::cout << "sum R: " << result.getResidualsSum() << std::endl;
			std::cout << "min B: ";
			result.getOLSEstimate().t().raw_print();
			std::cout <<  std::endl;
		}

		BSACache* cache = NULL;

		if( cacheSw.getValue() ) {
			cache = new BSACacheHashMap();
			std::cout << "Cache: HashMap" << std:: endl;

		} else {
			cache = new BSACacheNull();
			std::cout << "Cache: none" << std:: endl;
		}

		if( std::find( algVec.begin(), algVec.end(), "BSA" ) != algVec.end() ) {
			std::cout << "BSA Algorithm START." << std::endl;

			LTSAlgorithmBSA bsa = LTSAlgorithmBSA(
									  notrimArg.getValue(),
									  explVars,
									  respVar,
									  *cache,
									  interceptSw.getValue()
								  );
			bsa.solve( result );

			std::cout << "Result: " << std::endl;
			std::cout << "min w: ";
			result.getVector().t().raw_print();
			std::cout << "sum R: " << result.getResidualsSum() << std::endl;
			std::cout << "min B: ";
			result.getOLSEstimate().t().raw_print();
			std::cout <<  std::endl;
		}

		if( std::find( algVec.begin(), algVec.end(), "BSAM" ) != algVec.end() ) {
			// check for lowH
			if( lowNotrimArg.getValue() <= 0 || lowNotrimArg.getValue() > explVars.n_rows ) {
				throw TCLAP::ArgException( "Number of lower boundary of not-trimmed explanatory variables is required and must be positive integer and must no be higher that number of rows.", "lownottrimmed" );
			}

			if( lowNotrimArg.getValue() >= notrimArg.getValue() ) {
				throw TCLAP::ArgException( "Lower boundary of not-trimmed explanatory variables cannot be higher that high boundary.", "lownottrimmed" );
			}

			std::cout << "BSAM Algorithm START." << std::endl;

			BSACache* cache = NULL;

			if( cacheSw.getValue() ) {
				cache = new BSACacheHashMap();
			} else {
				cache = new BSACacheNull();
			}

			LTSAlgorithmBSAM bsa = LTSAlgorithmBSAM(
									   lowNotrimArg.getValue(),
									   notrimArg.getValue() + 1,
									   explVars,
									   respVar,
									   *cache,
									   interceptSw.getValue()
								   );
			bsa.solve( result );

			std::vector<LTSResult> results;
			bsa.getResultsByH( results );

			for( unsigned int i = 0; i < results.size(); ++i ) {
				std::cout << "Result: "  << ( i + lowNotrimArg.getValue() ) << std::endl;
				std::cout << "min w: ";
				results[i].getVector().t().raw_print();
				std::cout << "sum R: " << results[i].getResidualsSum() << std::endl;
				std::cout << "min B: ";
				results[i].getOLSEstimate().t().raw_print();
			}

			std::cout <<  std::endl;
		}

		delete cache;

	} catch( TCLAP::ArgException& e ) { // catch any parse exceptions
		std::cerr << "error: " << e.error() << " for argument " << e.argId() << std::endl;
		return ERR_FAILURE;
	}

	return ERR_SUCCESS;
}

bool parseFile( const std::string& filename, bool intercept, bool hasID , arma::mat& data ) {
	std::ifstream inFile( filename.c_str() );

	std::string line;

	if( inFile.is_open() ) {
		while( !inFile.eof() ) {
			getline( inFile, line );

			// remove trailing whitespace
			const char* const whitespace = " \t\r\n\v\f";
			line.erase( line.find_last_not_of( whitespace ) + 1 );

			double tmp;
			std::vector<double> tmprow;
			std::stringstream s( line );

			// last line if EOF after newline
			if( line.empty() ) {
				break;
			}

			for( int l = 0 ; !s.eof(); l++ ) {
				s >> tmp;
				//TODO fail check

				if( intercept && ( l == 0 ) ) {
					tmprow.push_back( 1 );
				}

				if( hasID && ( l == 0 ) ) {
					// skip first column if it's ID
				} else {
					tmprow.push_back( tmp );
				}
			}

			arma::rowvec r( tmprow );
			data = arma::join_cols( data, r );
		}

		inFile.close();
		return true;
	}

	return false;
}

