#ifndef LTSALGORITHM_ADSUAKSYVKASS1235
#define LTSALGORITHM_ADSUAKSYVKASS1235

#include "armadillo"

#include "LTSResult.h"
/// <summary>
/// Struct for holding relation between row and computed residual.
/// </summary>
struct LTSResidual {
	/// <summary>
	/// Computed residual.
	/// </summary>
	double r;

	/// <summary>
	/// Row number.
	/// </summary>
	int row;
};

/// <summary>
/// Sorting method for residual struct. Sorting by residual value.
/// </summary>
bool LTSResidualSort( const LTSResidual& a, const LTSResidual& b );

/// <summary>
/// Abstract predecessor for LTS algorithms.
/// </summary>
class LTSAlgorithm {
	public:
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="hParam">Number of non-trimmed explanatory variables.</param>
		/// <param name="explVars">Matrix of explanatory variables.</param>
		/// <param name="respVar">Matrix (vector) of responce variables.</param>
		LTSAlgorithm( int hParam, arma::mat& explVars, arma::mat& respVar );
		virtual ~LTSAlgorithm() {};

		/// <summary>
		/// Method computing LTS estimation for data.
		/// </summary>
		/// <param name="outResult"> Output variable for result of LTS algorithm.</param>
		/// <returns>True if successful.</returns>
		virtual bool solve( LTSResult& outResult ) = 0;


	protected:
		int m_hParam;
		arma::mat& m_eVars;
		arma::mat& m_rVar;

		/// <summary>
		/// Compute residual value for given row.
		/// </summary>
		/// <param name="row">Row number</param>
		/// <param name="OLS">OLS estimate to compute residuals.</param>
		/// <returns>Squared residual value for given row.</returns>
		virtual double getResidual( const int row, const arma::vec& OLS ) const;

		/// <summary>
		/// Computes OLS estimate for given data
		/// </summary>
		/// <param name="w">Vector of values signifing presence (1) or absence (0) of measurement in OLS estimate.</param>
		/// <param name="outMat">Output matrix with OLS estimate for input measurements given by diagW.</param>
		virtual void getOLSEstimate( const arma::uvec& w, arma::mat& outMat ) const;

		/// <summary>
		/// Computes OLS estimate for given data
		/// </summary>
		/// <param name="posVec">Vector of rows.</param>
		/// <param name="outMat">Output matrix with OLS estimate for input measurements given by posVec.</param>
		virtual void getOLSEstimatePosVec( const arma::uvec& posVec, arma::mat& outMat ) const;
				
		/// <summary>
		/// Creates a new position vector from binary vector.
		/// </summary>
		/// <param name="v">Binary vector.</param>
		/// <returns>Vector of positions.<returns>
		arma::uvec binVectorToPos( const arma::uvec& v ) const;

		/// <summary>
		/// Creates a new binary vector from position vector.
		/// </summary>
		/// <param name="v">Position vector.</param>
		/// <param name="size">Size of resulting binary vector.</param>
		/// <returns>Binary vector.<returns>
		arma::uvec posVectorToBin( const arma::uvec& v, const int size ) const;
};

#endif // LTSALGORITHM_ADSUAKSYVKASS1235