#ifndef BSACACHEHASHMAP__SLJKDTYO3443949DFSKLJHFIUGAISD
#define BSACACHEHASHMAP__SLJKDTYO3443949DFSKLJHFIUGAISD

#include "boost/unordered_map.hpp"

#include "BSACache.h"
#include "BSAHashMapKey.h"

typedef boost::unordered_map< BSAHashMapKey, arma::mat, HashUvec<BSAHashMapKey>, EqualUvec<BSAHashMapKey> > HashMapUvec ;
/// <summary>
/// Implementation of cache interface using STD unordered_map.
/// </summary>
class BSACacheHashMap : public BSACache {
	public:
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="initSize">Initial number of buckets in map.</param>
		BSACacheHashMap( unsigned int initSize );
		/// <summary>
		/// Constructor.
		/// </summary>
		BSACacheHashMap();

		virtual bool get( const arma::uvec& w, arma::mat& outMat );
		virtual void insert( const arma::uvec& w, const arma::mat& OLS );

	private:
		HashMapUvec m_Cache;
		int m_CacheHitCnt;
		int m_CacheMissCnt;

};

#endif //BSACACHEHASHMAP__SLJKDTYO3443949DFSKLJHFIUGAISD