#include "LTSAlgorithm.h"

LTSAlgorithm::LTSAlgorithm( int hParam, arma::mat& explVars, arma::mat& respVar )
	: m_eVars( explVars ),
	  m_rVar( respVar ),
	  m_hParam( hParam ) { };

double LTSAlgorithm::getResidual( const int row, const  arma::vec& OLS ) const {
	arma::mat r = ( m_rVar( row ) - ( m_eVars.row( row ) * OLS ) );
	return ( r( 0, 0 ) * r( 0, 0 ) );
}

void LTSAlgorithm::getOLSEstimate( const arma::uvec& w, arma::mat& outMat ) const {
	arma::umat diagW = arma::umat().zeros( w.size(), w.size() );
	diagW.diag() = w;

	outMat = ( m_eVars.t()* diagW * m_eVars ).i( false ) * m_eVars.t() *diagW* m_rVar;
}

void LTSAlgorithm::getOLSEstimatePosVec( const arma::uvec& posVec, arma::mat& outMat ) const {
	arma::mat XX = m_eVars.rows( posVec ) ;
	arma::mat YY = m_rVar.rows( posVec );
	outMat = ( XX.t() * XX ).i( false ) * XX.t() * YY;

/*	arma::mat Q, R;
	arma::qr_econ(Q, R, XX);
	outMat = arma::solve(R, Q.t() * YY, true);*/
}

bool LTSResidualSort( const LTSResidual& a, const LTSResidual& b )	 {
	return ( a.r < b.r );
}

arma::uvec LTSAlgorithm::binVectorToPos( const arma::uvec& v ) const {
	std::vector<arma::uword> posV = std::vector<arma::uword>();

	for( unsigned int i = 0; i < v.size(); ++i ) {
		if( v( i ) == 1 ) {
			posV.push_back( i );
		}
	}

	return arma::uvec( posV );
}

arma::uvec LTSAlgorithm::posVectorToBin( const arma::uvec& v, const int size ) const {
	arma::uvec binV = arma::uvec(size);
	binV.fill(0);

	for( unsigned int i = 0; i < v.size(); ++i ) {
		binV( v( i ) ) = 1;
	}

	return binV;
}