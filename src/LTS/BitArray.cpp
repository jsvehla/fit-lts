#include <iostream>
#include <cmath>
#include <climits>
#include <stdexcept>

#include "BitArray.h"

BitArray::BitArray( unsigned int size ) {
	if( size == 0 ) {
		size = 1;
	}

	m_Size = size;
	m_ISize = (unsigned int) std::ceil( ( double ) size / ( CHAR_BIT * sizeof( int ) ) );
	m_Array = new int[m_ISize];

	resetArray();
	resetFlags();
}

BitArray::~BitArray() {
	delete[] m_Array;
}

void BitArray::resetArray() {
	for( unsigned int i = 0; i < m_ISize; ++i ) {
		m_Array[i] = 0;
	}
}

void BitArray::increment() {
	bool carry = false;

	for( unsigned int i = 0; i < m_ISize; ++i ) {
		carry = incrementSubArr( i );

		if( carry == false ) {
			break;
		}
	}

	if( carry == true ) { // overflow in full array
		m_Overflow = true;
	}

	if( ( m_Size % ( 8 * sizeof( int ) ) ) != 0 ) {
		// overflow in array size < sizof(int)*n
		unsigned int aPos = ( m_Size ) % ( CHAR_BIT * sizeof( int ) );
		bool overflow = ( m_Array[m_ISize - 1] >> aPos ) & 0x01;

		if( overflow ) {
			resetArray();
			m_Overflow = true;
		}
	}
}

inline bool BitArray::incrementSubArr( unsigned int arr ) {
	if( m_Array[arr] == UINT_MAX ) {
		m_Array[arr] = 0;
		return true;	// carry
	}

	m_Array[arr] += 1;
	return false;		// no carry
}

bool BitArray::getBit( const unsigned int pos ) const {
	if( pos >= m_Size ) {
		throw std::out_of_range( "Position out of range: " + pos );
	}

	unsigned int arr = pos / ( CHAR_BIT * sizeof( int ) );
	unsigned int aPos = pos % ( CHAR_BIT * sizeof( int ) );

	return ( m_Array[arr] >> aPos ) & 0x01;
}

void BitArray::setBit( const unsigned int pos ) {
	if( pos >= m_Size ) {
		throw std::out_of_range( "Position out of range: " + pos );
	}

	unsigned int arr = pos / ( CHAR_BIT * sizeof( int ) );
	unsigned int aPos = pos % ( CHAR_BIT * sizeof( int ) );

	m_Array[arr] |= ( 0x01 << aPos );
}

bool BitArray::getOverflowFl() {
	return m_Overflow;
}

void BitArray::resetFlags() {
	m_Overflow = false;
}

bool BitArray::operator==( const BitArray& other ) const {
	if( other.m_Size != m_Size ) {
		return false;
	}

	for( unsigned int i = 0; i < m_ISize; ++i ) {
		if( m_Array[i] != other.m_Array[i] ) {
			return false;
		}
	}

	return true;
}
