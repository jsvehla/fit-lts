#include "LTSResult.h"

void LTSResult::setOLSEstimate( const arma::vec& OLS ) {
	m_OLS = arma::vec( OLS );
}

void LTSResult::setResidualsSum( double resSum ) {
	m_Residuals = resSum;
}

void LTSResult::setVector( const arma::uvec& w ) {
	m_Vector = arma::uvec( w );
}

double LTSResult::getResidualsSum() const {
	return m_Residuals;
}

arma::vec LTSResult::getOLSEstimate() const {
	return m_OLS;
}

arma::uvec LTSResult::getVector() const {
	return m_Vector;
}