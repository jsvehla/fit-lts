#ifndef LTSRESULT_DIERNFDUGDSLDSLDFUIE
#define LTSRESULT_DIERNFDUGDSLDSLDFUIE

#include "armadillo"


/// <summary>
/// Class for storing results of LTS algorithm run.
/// </summary>
class LTSResult {

	public:
		LTSResult() {};
		virtual ~LTSResult() {};

		/// <summary>
		/// Setter for sum of residuals.
		/// </summary>
		/// <param name="residual">Sum of residuals values.</param>
		void setResidualsSum( double residual );

		/// <summary>
		/// Setter for vector of included measurements.
		/// </summary>
		/// <param name="w">Vector indicating presence of measurement in LTS estimate.</param>
		void setVector( const arma::uvec& w );

		/// <summary>
		/// Setter for LTS estimate.
		/// </summary>
		/// <param name="OLS">LTS estimate.</param>
		void setOLSEstimate( const arma::vec& OLS );

		/// <summary>
		/// Getter for sum of residuals.
		/// </summary>
		/// <returns>Sum of residuals values.</returns>
		double getResidualsSum() const;

		/// <summary>
		/// Getter for vector of measurements.
		/// </summary>
		/// <returns>Vector indicating presence of measurement in LTS estimate.</returns>
		arma::uvec getVector() const;

		/// <summary>
		/// Getter for best OLS estimate of h measurements from given data.
		/// </summary>
		/// <returns>OLS estimate.</returns>
		arma::vec getOLSEstimate() const;

	private:
		double m_Residuals;
		arma::uvec m_Vector;
		arma::vec m_OLS;
};


#endif //LTSRESULT_DIERNFDUGDSLDSLDFUIE