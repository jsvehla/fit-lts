#include <iostream>

#include "BSACacheHashMap.h"

BSACacheHashMap::BSACacheHashMap( unsigned int initSize )
	: m_CacheHitCnt( 0 ),
	  m_CacheMissCnt( 0 ),
	  m_Cache( HashMapUvec() ) {
	m_Cache.rehash( initSize );
}

BSACacheHashMap::BSACacheHashMap()
	: m_CacheHitCnt( 0 ),
	  m_CacheMissCnt( 0 ),
	  m_Cache( HashMapUvec() ) {
}

bool BSACacheHashMap::get( const arma::uvec& w, arma::mat& outMat ) {
	BSAHashMapKey key = BSAHashMapKey( w );
	key.setHash( key.getHash() );
	HashMapUvec::const_iterator it = m_Cache.find( key );

	if( it == m_Cache.end() ) {
		++m_CacheMissCnt;
		return false;
	}

#ifdef _DEBUG
	std::cout << "CHit:  ";
	//arma::sort( ( it->first.getVector() ).t(), 0, 1 ).raw_print();
	( it->first.getVector() ).t().raw_print();
	it->second .t().raw_print();
#endif

	++m_CacheHitCnt;
	outMat = it->second;
	return true;
}

void BSACacheHashMap::insert( const arma::uvec& w, const arma::mat& OLS ) {
	BSAHashMapKey key = BSAHashMapKey( w );
	key.setHash( key.getHash() );
	m_Cache.insert( std::make_pair<BSAHashMapKey, arma::mat>( key, OLS ) );
}
