#ifndef BSACACHENULL__LESY30299280WOIIFSAHLH3873
#define BSACACHENULL__LESY30299280WOIIFSAHLH3873

#include "BSACache.h"
/// <summary>
/// Implementation of cache interface as Null object pattern. Doesn't store vectors.
/// </summary>
class BSACacheNull : public BSACache {
	public:
		virtual bool get( const arma::uvec& w, arma::mat& outMat );
		virtual void insert( const arma::uvec& w, const arma::mat& OLS );
};

#endif //BSACACHENULL__LESY30299280WOIIFSAHLH3873