#ifndef LTSALGORITHMBSA_SAMFHLDAKSHFSLKADS
#define LTSALGORITHMBSA_SAMFHLDAKSHFSLKADS

#include "armadillo"

#include "LTSAlgorithm.h"
#include "LTSAlgorithmBSAM.h"
#include "BitArray.h"
#include "BSACache.h"

#define ERR_DOUBLE 0.0001

/// <summary>
/// Implementation of BSA LTS algorithm for robust regression.
/// </summary>
class LTSAlgorithmBSA : public LTSAlgorithm {
	public:

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="hParam">Number of non-trimed measurements.</param>
		/// <param name="explVars">Matrix of explanatory variables.</param>
		/// <param name="respVar">Vector of responce variable.</param>
		/// <param name="cache">Cache class to store intermediate results.</param>
		/// <param name="intercept">Flag signifiing presence of intercept column in data. Setting to true will cause algorithm to skip equations that are guaranteed not to have solution. If unsure, set to FALSE.</param>
		LTSAlgorithmBSA( int hParam, arma::mat& explVars, arma::mat& respVar, BSACache& cache, bool intercept = false );
		~LTSAlgorithmBSA();

		virtual bool solve( LTSResult& outResult );

	private:
		LTSAlgorithmBSAM* m_Core;
};

#endif //LTSALGORITHMBSA_SAMFHLDAKSHFSLKADS