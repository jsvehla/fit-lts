#ifndef BIT_ARRAY__SDLHRESLLDNFLDSHFLDHFLDSLFDSFDSFD
#define BIT_ARRAY__SDLHRESLLDNFLDSHFLDHFLDSLFDSFDSFD

/// <summary>
/// BitArray class for holding array of bits. Customized to fit library needs.
/// </summary>
class BitArray {
	public:
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="size">Number of bits to be stored</param>
		BitArray( unsigned int size );
		~BitArray();

		/// <summary>
		/// Increments bit array by one. Follows same rules as a binary number.
		/// </summary>
		void increment();

		/// <summary>
		/// Resets all bits to zero.
		/// </summary>
		void resetArray();
		/// <summary>
		/// Resets all flags.
		/// </summary>
		void resetFlags();
		/// <summary>
		/// Overflow flag.
		/// </summary>
		/// <returns>True if last increment caused arrya to overflow.</returns>
		bool getOverflowFl();

		/// <summary>
		/// Get value of bit at position.
		/// </summary>
		/// <param name="position">Position of bit</param>
		/// <returns>Binary value of bit at position</returns>
		bool getBit( const unsigned int position ) const;
		/// <summary>
		/// Set value of bit at position to 1.
		/// </summary>
		/// <param name="position">Position of bit</param>
		void setBit( const unsigned int position );

		/// <summary>
		/// Compares two arrays.
		/// </summary>
		/// <param name="other">Second array</param>
		/// <returns>True if same</returns>
		bool operator== ( const BitArray& other ) const;

	private:
		bool incrementSubArr( unsigned int arr );

		int* m_Array;
		unsigned int m_Size;
		unsigned int m_ISize;
		bool m_Overflow;
};

#endif