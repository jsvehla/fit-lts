#ifndef LTSALGORITHMVA__aSDJELDSHEJGF5876HASK
#define LTSALGORITHMVA__aSDJELDSHEJGF5876HASK

#include "armadillo"

#include "LTSAlgorithm.h"

/// <summary>
/// Implementation of VA probability algorithm for robust linear regression
/// </summary>
class LTSAlgorithmVA : public LTSAlgorithm {
	public:

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="kParam">Initial number of measurement for start of each cycle. Reccommened value: p+1.</param>
		/// <param name="hParam">Number of non-trimmed measurements.</param>
		/// <param name="cycles">Number of cycles. Higher number has higher probability of finding optimal solution.</param>
		/// <param name="explVars">Matrix of explanatory variables</param>
		/// <param name="respVar">Vector of responce variable.</param>
		LTSAlgorithmVA( int kParam, int hParam, int cycles, arma::mat& explVars, arma::mat& respVar );

		virtual bool solve( LTSResult& outResult );


	private:
		bool getRandomW( arma::uvec& outW ) const;
		bool compareVectors( const arma::uvec& w, const arma::uvec& ww ) const;

		int m_Cycles;
		int m_kParam;
};

#endif // LTSALGORITHMVA__aSDJELDSHEJGF5876HASK