#include <ctime>
#include <vector>
#include <algorithm>
#include <limits>

#include "LTSAlgorithmVA.h"

LTSAlgorithmVA::LTSAlgorithmVA( int kParam, int hParam, int cycles, arma::mat& explVars, arma::mat& respVar )
	: LTSAlgorithm( hParam, explVars, respVar ),
	  m_kParam( kParam ),
	  m_Cycles( cycles ) { };

bool LTSAlgorithmVA::solve( LTSResult& outResult ) {
	srand( time( NULL ) ); // TODO better place to seed random (use boost?)

	double minR = std::numeric_limits<double>::max();
	arma::uvec minW;
	arma::vec minB;

	for( int iter = 0; iter < m_Cycles; iter++ ) {
		// random diagonal
		arma::uvec w = arma::uvec( m_eVars.n_rows );
		getRandomW( w );
		arma::vec B;

		while( true ) {
			try {
				getOLSEstimatePosVec( binVectorToPos( w ) , B );

			} catch( std::runtime_error ) {
				// catch singular matrix inversion (property of data??)
				break;
			}

			// residuals
			std::vector<LTSResidual> r = std::vector<LTSResidual>();

			for( unsigned int row = 0; row < m_eVars.n_rows; row++ ) {
				LTSResidual tmp;
				tmp.r = getResidual( row, B );
				tmp.row = row;
				r.push_back( tmp );
			}

			// sort and create new w
			std::sort( r.begin(), r.end(), LTSResidualSort );
			arma::uvec ww = arma::uvec( m_eVars.n_rows );
			ww.fill( 0 );

			double sumR = 0;

			for( int i = 0; i < m_hParam; i++ ) {
				ww( r[i].row ) = 1;
				sumR += r[i].r;
			}

			// check for best current solution
			if( sumR < minR ) {
				minR = sumR;
				minW = ww;
				minB = B;
			}

#ifdef _DEBUG
			std::cout << "w:  ";
			ww.t().raw_print();
#endif

			// compare new better w with previous w and break if same
			if( compareVectors( ww, w ) ) {
				break;

			} else {
				w = ww;
			}
		}

#ifdef _DEBUG
		B.raw_print();
		std::cout << " -- " << std::endl;
#endif
	}

	outResult.setOLSEstimate( minB );
	outResult.setResidualsSum( minR );
	outResult.setVector( minW );
	return true;
}

bool LTSAlgorithmVA::getRandomW( arma::uvec& outW ) const {
	if( outW.size() != m_eVars.n_rows ) {
		return false;
	}

	outW.fill( 0 );

	for( int i = 0; i < m_kParam; i++ ) {
		int r = ( rand() % m_eVars.n_rows );

		if( outW( r ) == 1 ) {
			--i;
			continue; // skip this run
		}

		outW( r ) = 1;
	}

#ifdef _DEBUG
	std::cout << "rw: ";
	outW.t().raw_print();
#endif
	return true;
}

bool LTSAlgorithmVA::compareVectors( const arma::uvec& ww, const arma::uvec& w ) const {
	for( unsigned int i = 0; i < ww.size(); i++ ) {
		if( ww( i ) != w( i ) ) {
			return false;
		}
	}

	return true;
}
