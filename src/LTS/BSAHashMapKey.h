#ifndef BSAHASHMAP_KEY__EUAYR73T73R7ET389QIGSAFDUTRZXVT3
#define BSAHASHMAP_KEY__EUAYR73T73R7ET389QIGSAFDUTRZXVT3

#include "armadillo"


template<class T> class HashUvec;
template<class T> class EqualUvec;

/// <summary>
/// Key for hash map in BSACacheHashMap. Implemented to work with binary vectors.
/// </summary>
class BSAHashMapKey {
	public:
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="vec">Vector being represented by this key.</param>
		BSAHashMapKey( const arma::uvec& vec );

		/// <summary>
		/// Returns a associated hash for a vector.
		/// </summary>
		/// <returns>Hash of the associated vector.<returns>
		size_t getHash() const;
		
		/// <summary>
		/// Set an associated hash for the vector. To prevent the hash to be computed multiple times during
		/// Hash map operations, hash is stored.
		/// </summary>
		/// <param name="hash">Hash value.</param>
		void setHash( const size_t hash );

		/// <summary>
		/// Compares vectors associated with a key. Vectors with length <= CHAR_BIT * sizeof(size_t) are compared by hash only.
		/// </summary>
		/// <param name="other">Vector.</param>
		/// <returns>True if same.<returns>
		bool isSame( const BSAHashMapKey& other ) const;

		/// <summary>
		/// Returns the associated vector.
		/// </summary>
		/// <returns>Binary vector.<returns>
		arma::uvec getVector() const;

	private:
		arma::uvec m_Vector;
		size_t m_Hash;
		bool m_isHashSet;
};

/// <summary>
/// Hash template specialization for binary vector.
/// </summary>
template<>
class HashUvec<BSAHashMapKey> {
	public:
		/// <summary>
		/// Computes hash.
		/// </summary>
		/// <returns>Hash.<returns>
		size_t operator()( const BSAHashMapKey& uv ) const {
			return uv.getHash();
		}
};

/// <summary>
/// Equals template specialization for binary vector.
/// </summary>
template<>
class EqualUvec<BSAHashMapKey> {
	public:
		/// <summary>
		/// Compares vectors.
		/// </summary>
		/// <param name="u1">First vector.</param>
		/// <param name="u2">Second vector.</param>
		/// <returns>True if same.<returns>
		bool operator()( const BSAHashMapKey& u1, const BSAHashMapKey& u2 ) const {
			return u1.isSame( u2 );
		}
};

#endif // BSAHASHMAP_KEY__EUAYR73T73R7ET389QIGSAFDUTRZXVT3