#include "BSAHashMapKey.h"

BSAHashMapKey::BSAHashMapKey( const arma::uvec& vec ) :
	m_Vector( vec ),
	m_Hash( 0 ),
	m_isHashSet( false ) { }

size_t	BSAHashMapKey::getHash() const {
	//return 1;
	if( m_isHashSet == true ) {
		return m_Hash;
	}

	int size = sizeof( size_t ) * CHAR_BIT;
	size_t hash = 0;

	// hash is represented as binary number, cycle if size_t too small
	for( unsigned int i = 0; i < m_Vector.size(); ++i ) {
//		hash |= ( 0x01 << ( m_Vector( i ) % size ) );
		if( m_Vector(i) == 1 ) {
			hash |= ( 0x01 << (  i % size ) );
		}
	}

	return hash;
}

void BSAHashMapKey::setHash( size_t hash ) {
	m_Hash = hash;
	m_isHashSet = true;
}

bool BSAHashMapKey::isSame( const BSAHashMapKey& other ) const {
	if( other.m_Vector.size() != m_Vector.size()) return false;
	// if the vector is smaller that size_t, we can directly compare hash
	if( ( m_Vector.size() <= ( sizeof( size_t ) * CHAR_BIT ) ) && ( m_isHashSet == true ) ) {
		return ( m_Hash == other.m_Hash );
	}

	// else compare normal  !!BUG - need sort for pos vector
	for( unsigned int i = 0; i < m_Vector.size(); ++i ) {
		if( m_Vector( i ) != other.m_Vector( i ) ) {
			return false;
		}
	}

	return true;
}


arma::uvec BSAHashMapKey::getVector() const {
	return m_Vector;
}