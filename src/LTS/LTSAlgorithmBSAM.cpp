#include <cmath>
#include <vector>
#include <algorithm>
#include <cmath>

#include "LTSAlgorithmBSAM.h"
#include "Combination.h"


LTSAlgorithmBSAM::LTSAlgorithmBSAM( int hParamL, int hParamH, arma::mat& explVars, arma::mat& respVar,	BSACache& cache, 	bool intercept )
	: LTSAlgorithm( hParamL, explVars, respVar ),
	  m_Intercept( intercept ),
	  m_hParamHigh( hParamH ),
	  m_Cache( cache ) {
	init();
}

void LTSAlgorithmBSAM::init( ) {
	// prepare array to store results
	int hs = m_hParamHigh - m_hParam;
	m_AllResults = new LTSResult[hs];

	for( int i = 0; i < hs; ++i ) {
		m_AllResults[i].setResidualsSum( std::numeric_limits<double>::max() );
	}
}

LTSAlgorithmBSAM::~LTSAlgorithmBSAM() {
	delete[] m_AllResults;
}

int LTSAlgorithmBSAM::compareDouble( const double a, const double b, const double err ) const {
	/* TODO: better comparsion */
	if( std::abs( a - b ) <= err ) {
		return 0;
	}

	if( a > b ) {
		return 1;
	}

	return -1;
}

bool LTSAlgorithmBSAM::solve( LTSResult& outResult ) {

	outResult.setResidualsSum( std::numeric_limits<double>::max() );

	Combinations p1 = Combinations( m_eVars.n_cols + 1, m_eVars.n_rows );
	std::vector<int> vk = std::vector<int>();

	while( p1.hasNext() ) {
		p1.getNext( vk );

#ifdef _DEBUG
		//std::cout << "Current measurement: ";

		//for( unsigned int i = 0; i < vk.size(); ++i ) {
		//	std::cout << vk[i] << " ";
		//}

		//std::cout << std::endl;
#endif

		// do 2^(m_eVars.n_cols) cycles
		BitArray signs( m_eVars.n_cols );

		if( m_Intercept ) {
			signs.increment();
		}

		for( ; !signs.getOverflowFl(); signs.increment() ) {

			//for (int i = 0; i < 10; ++i ){
			arma::vec B;

			if(	createAndSolveSystem( signs, vk, B ) ) {

				// first residual
				LTSResidual r0;
				r0.r   = getResidual( vk[0], B );
				r0.row = vk[0];

				std::vector<LTSResidual> r = std::vector<LTSResidual>();
				computeAndOrderResidual( B, r );

				// do for multiple h
				for( int hh = m_hParam; hh < m_hParamHigh; ++hh ) {
					if( ( compareDouble( r0.r, r[hh - 1].r, ERR_DOUBLE ) == 0 ) &&
							( compareDouble( r[hh - 1].r, r[hh].r, ERR_DOUBLE ) == 0 ) ) {
						// array starts at 0 -> compare h-1 and h (th) sorted residual
						// if true, B belongs to Hp as described in algorithm proof

						std::vector<LTSResidual> wPart = std::vector<LTSResidual>(); /* residuals lower that r0 */
						std::vector<LTSResidual> hPart = std::vector<LTSResidual>(); /* residuals equals to r0 */
						splitResiduals( r0, r, wPart, hPart );

						if( hPart.size() != m_eVars.n_cols + 1 ) {
							continue;
						}

						findSmallestOLSfromIntersect( hh, wPart, hPart, m_AllResults[hh - m_hParam] );

						LTSResult res = m_AllResults[hh - m_hParam];

						// set global minimum
						if( res.getResidualsSum() < outResult.getResidualsSum() ) {
							outResult.setResidualsSum( res.getResidualsSum() );
							outResult.setVector( res.getVector() );
							outResult.setOLSEstimate( res.getOLSEstimate() );
						}
					}
				}
			}
		}
	}

	return true;
}

void LTSAlgorithmBSAM::computeAndOrderResidual(	const arma::vec& B, std::vector<LTSResidual>& outSorted ) const {
	LTSResidual tmp;

	for( unsigned int row = 0; row < m_eVars.n_rows; row++ ) {

		tmp.r = getResidual( row, B );
		tmp.row = row;
		outSorted.push_back( tmp );
	}

	std::sort( outSorted.begin(), outSorted.end(), LTSResidualSort );
}

bool LTSAlgorithmBSAM::createAndSolveSystem( const BitArray& signs, const std::vector<int>& rows, arma::vec& outB ) const {
	arma::mat BX = arma::mat( m_eVars.n_cols, m_eVars.n_cols );
	arma::mat BY = arma::mat( m_eVars.n_cols, 1 );

	for( unsigned int j = 0; j < m_eVars.n_cols; ++j ) {
		bool sign = signs.getBit( j );

		if( sign ) {
			BX.row( j ) = m_eVars.row( rows[0] ) + m_eVars.row( rows[j + 1] );
			BY.row( j ) = m_rVar.row( rows[0] ) + m_rVar.row( rows[j + 1] );

		} else {
			BX.row( j ) = m_eVars.row( rows[0] ) - m_eVars.row( rows[j + 1] );
			BY.row( j ) = m_rVar.row( rows[0] ) - m_rVar.row( rows[j + 1] );
		}
	}

	try {
		outB = arma::solve( BX, BY );

	} catch( std::runtime_error ) {
		// no solution
		return false;
	}

	return true;
}

double LTSAlgorithmBSAM::computeHSum( const arma::uvec& w, const arma::mat& OLS ) const {
	//arma::umat diagW = arma::umat().zeros( w.size(), w.size() );
	//diagW.diag() = w;

	arma::mat tmp = ( m_rVar - m_eVars * OLS );
	tmp = tmp.rows( w );

	double sumR = arma::norm( tmp , 2 );
	//double sumR = arma::norm( diagW *( m_rVar - m_eVars * OLS ) , 2 );

	return sumR * sumR;
}

void LTSAlgorithmBSAM::splitResiduals( const LTSResidual& r0, const std::vector<LTSResidual>& r, std::vector<LTSResidual>& outCommonPart, std::vector<LTSResidual>& outCombPart ) const {
	for( unsigned int i = 0; i < r.size(); ++i ) {
		if( compareDouble( r0.r, r[i].r, ERR_DOUBLE ) > 0 ) {
			outCommonPart.push_back( r[i] );

		} else if( compareDouble( r0.r, r[i].r, ERR_DOUBLE ) == 0 ) {
			outCombPart.push_back( r[i] );
		}
	}
}

bool LTSAlgorithmBSAM::findSmallestOLSfromIntersect( const int hParam, const std::vector<LTSResidual>& commonPart,	const std::vector<LTSResidual>& combPart, LTSResult& outResult ) {
	Combinations h1 = Combinations( hParam - commonPart.size(), combPart.size() );
	std::vector<int> ww = std::vector<int>();

	arma::uvec w = arma::uvec( hParam );
	//arma::uvec w = arma::uvec( m_eVars.n_rows );
	
	while( h1.hasNext() ) {
		h1.getNext( ww );
		//w.fill( 0 );

		for( unsigned int i = 0; i < commonPart.size(); ++i ) { 	// fill common part
			//w(commonPart[i].row) = 1;
			w( i ) = commonPart[i].row;
		}

		for( unsigned int i = 0; i < ww.size(); ++i ) { 			// use combination to fill rest
			//w( combPart[ww[i]].row ) = 1;
			w( i + commonPart.size() ) = combPart[ww[i]].row;
		}

#ifdef _DEBUG
	//	std::cout << "diagW: ";
//		sort( w.t(), 0, 1 ).raw_print();
	//	w.t().raw_print();
#endif

		arma::vec OLS;

		try {
			getOLSEstimate( w ,  OLS );

		} catch( std::runtime_error ) {
			// catch singular matrix inversion (property of data??)
			return false;
		}

		// compute residuals for OLSEstimate
		double sumR = computeHSum( w, OLS );

		if( sumR < outResult.getResidualsSum() ) {
			outResult.setResidualsSum( sumR );
			outResult.setVector( posVectorToBin( w, m_eVars.n_rows ) );
			//outResult.setVector( w);
			outResult.setOLSEstimate( OLS );

#ifdef _DEBUG
	//		std::cout << "New min w:    ";
//			outResult.getVector().t().raw_print();
//			std::cout << "New min B:    ";
	//		outResult.getOLSEstimate().t().print();
	//		std::cout << "New min sumR: " << outResult.getResidualsSum() << std::endl << std::endl;
#endif
		}
	}

	return true;
}

void LTSAlgorithmBSAM::getResultsByH( std::vector<LTSResult>& outResults ) const {
	outResults.clear();

	LTSResult tmp;
	int hs = m_hParamHigh - m_hParam;

	for( int i = 0; i < hs; ++i ) {
		tmp.setOLSEstimate( m_AllResults[i].getOLSEstimate() );
		tmp.setResidualsSum( m_AllResults[i].getResidualsSum() );
		tmp.setVector( m_AllResults[i].getVector() );
		outResults.push_back( tmp );
	}
}

void LTSAlgorithmBSAM::getOLSEstimate( const arma::uvec& w, arma::mat& outMat ) {
	arma::uvec ww = posVectorToBin(w, m_eVars.n_rows);
	if( m_Cache.get( ww, outMat ) == true ) {
		return;
	}

	LTSAlgorithm::getOLSEstimatePosVec( w, outMat );
	//LTSAlgorithm::getOLSEstimate( w, outMat );
	m_Cache.insert( ww, outMat );
}