#ifndef COMBINATIONS_HSITSUELEDPASEFLKDSFHSLDFU
#define COMBINATIONS_HSITSUELEDPASEFLKDSFHSLDFU

#include <vector>

/// <summary>
/// Combinations class to provide easy generation of combination
/// </summary>
class Combinations {
	public:
		/// <summary>
		/// Constructor. C(k,n).
		/// </summary>
		/// <param name="k"> Number of things to be selected.</param>
		/// <param name="n"> Number of things to be selected from.</param>
		Combinations( int k, int n );
		~Combinations();

		/// <summary>
		/// Return next possible conbination. Returns first possible on first call.
		/// </summary>
		/// <param name="outComb">Output vector filled with next combination after call.</param>
		/// <returns>True if <paramref name="outComb"/> contains next combination.</returns>
		bool getNext( std::vector<int>& outComb );

		/// <summary>
		/// Check if instance can generate next combination (last generated was las possible.
		/// </summary>
		/// <returns> True if more combinations exist.</returns>
		bool hasNext() const;

	private:
		int m_K;
		int m_N;

		int* m_S;
};

#endif // COMBINATIONS_HSITSUELEDPASEFLKDSFHSLDFU