#include "LTSAlgorithmBSA.h"

LTSAlgorithmBSA::LTSAlgorithmBSA( int hParam, arma::mat& explVars, arma::mat& respVar, BSACache& cache, bool intercept )
	: LTSAlgorithm( hParam, explVars, respVar ) {
	m_Core = new LTSAlgorithmBSAM( hParam, hParam + 1, explVars, respVar, cache, intercept );
}

LTSAlgorithmBSA::~LTSAlgorithmBSA() {
	delete m_Core;
}

bool LTSAlgorithmBSA::solve( LTSResult& outResult ) {
	return m_Core->solve( outResult );
}