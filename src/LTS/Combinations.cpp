#include "Combination.h"

Combinations::Combinations( int k, int n )
	: m_K( k ), m_N( n ) {
	m_S = new int[m_K];

	for( int i = 0; i < m_K; ++i ) {
		m_S[i] = i;
	}

	//Force first getNext to return initial combination
	m_S[m_K - 1] -= 1;
}

Combinations::~Combinations() {
	delete[] m_S;
}

bool Combinations::getNext( std::vector<int>& outComb ) {
	if( !hasNext() ) {
		return false;
	}

	int m = ( m_K - 1 );
	int mVal = ( m_N - 1 );

	while( m_S[m] == mVal ) {
		m -= 1;
		mVal -= 1;
	}

	m_S[m] += 1;

	for( int j  = m + 1; j < m_K; ++j ) {
		m_S[j] = m_S[j - 1] + 1;
	}

	outComb.clear();

	for( int i = 0; i < m_K; ++i ) {
		outComb.push_back( m_S[i] );
	}

	return true;
}

bool Combinations::hasNext() const {
	return ( m_S[0] != ( m_N - m_K ) );
}