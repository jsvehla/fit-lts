#ifndef BSACACHE__DSJHKJHGSOIUFP9ER03853095394032
#define BSACACHE__DSJHKJHGSOIUFP9ER03853095394032

#include "armadillo"
/// <summary>
/// Cache interface for BSA algorithm.
///	Serves to store computed values to avoid multiple OLS computations for same data.
/// Depending on implementation of algebraic operation, its usefulness may vary.
/// </summary>
class BSACache {
	public:
		/// <summary>
		/// Get a vector from cache. If no vector is found, outMat is not changed.
		/// </summary>
		/// <param name="w">Vector.</param>
		/// <param name="outMat">Associated value.</param>
		/// <returns>True if vector was found. False otherwise.</returns>
		virtual bool get( const arma::uvec& w, arma::mat& outMat ) = 0;

		/// <summary>
		/// Inserts a vector into cache.
		/// </summary>
		/// <param name="w">Vector.</param>
		/// <param name="OLS">Associated value.</param>
		virtual void insert( const arma::uvec& w, const arma::mat& OLS ) = 0;
};

#endif