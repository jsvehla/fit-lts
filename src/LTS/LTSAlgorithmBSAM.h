#ifndef LTSALGORITHMBSAM_JKSDHKEPOSFLKNKHUFASIUGE
#define LTSALGORITHMBSAM_JKSDHKEPOSFLKNKHUFASIUGE

#include "armadillo"

#include "LTSAlgorithm.h"
#include "BitArray.h"
#include "BSACache.h"
#include "BSACacheNull.h"

#define ERR_DOUBLE 0.0001

/// <summary>
/// Implementation of BSA LTS algorithm for robust regression.
/// </summary>
class LTSAlgorithmBSAM : public LTSAlgorithm {
	public:
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="hParamL">Number of non-trimed measurements - lower bound.</param>
		/// <param name="hParamH">Number of non-trimed measurements - upper bound, exclusive.</param>
		/// <param name="explVars">Matrix of explanatory variables.</param>
		/// <param name="respVar">Vector of responce variable.</param>
		/// <param name="cache">Cache class to store intermediate results.</param>
		/// <param name="intercept">Flag signifiing presence of intercept column in data. Setting to true will cause algorithm to skip equations that are guaranteed not to have solution. If unsure, set to FALSE.</param>
		LTSAlgorithmBSAM( int hParamL, int hParamH, arma::mat& explVars, arma::mat& respVar, BSACache& cache, bool intercept = false );
		~LTSAlgorithmBSAM();
		/// <summary>
		/// Method computing LTS estimation for data. In case of BSAM algorithm, it computes overall minimum for all sizes of result vectors (h). To get LTS estimate for each h separately, see <see cref="getAllResults" />.
		/// </summary>
		/// <param name="outResult"> Global minimum of all LTS estimates.</param>
		/// <returns>True if successful.</returns>
		virtual bool solve( LTSResult& outResult );
		/// <summary>
		///	Gets a vector of LTS estimates for each h param from interval <hParamL, hParamH).
		/// </summary>
		/// <param name="outResults">Vector which will be filled only with the results.</param>
		void getResultsByH( std::vector<LTSResult>& outResults ) const;

	private:

		void getOLSEstimate(
			const arma::uvec& w,
			arma::mat& outMat
		);

		void init( );

		bool findSmallestOLSfromIntersect(
			const int hParam,
			const std::vector<LTSResidual>& commonPart,
			const std::vector<LTSResidual>& combPart,
			LTSResult& outResult
		);

		int compareDouble(
			const double a,
			const double b,
			const double err
		) const;

		void computeAndOrderResidual(
			const arma::vec& B,
			std::vector<LTSResidual>& outSorted
		) const;

		bool createAndSolveSystem(
			const BitArray& signs,
			const std::vector<int>& combination,
			arma::vec& outB
		) const;

		double computeHSum(
			const arma::uvec& w,
			const arma::mat& OLS
		) const;

		void splitResiduals(
			const LTSResidual& r0,
			const std::vector<LTSResidual>& r,
			std::vector<LTSResidual>& outCommonPart,
			std::vector<LTSResidual>& outCombPart
		) const;

		/* members */
		bool m_Intercept;
		int m_hParamHigh;
		LTSResult* m_AllResults;

		BSACache& m_Cache;
};

#endif //LTSALGORITHMBSAM_JKSDHKEPOSFLKNKHUFASIUGE