#ifndef TYPEDEFS_HAIOEMVUEk65df6df
#define TYPEDEFS_HAIOEMVUEk65df6df

#include "armadillo"

#ifdef USE_TR1
#include <tr1\memory>
typedef std::tr1::shared_ptr<arma::mat> PMAT;
typedef std::tr1::shared_ptr<arma::rowvec> PROW;
typedef std::tr1::shared_ptr<arma::vec> PVEC;
#else
#include <memory>
typedef std::shared_ptr<arma::mat> PMAT;
typedef std::shared_ptr<arma::rowvec> PROW;
typedef std::shared_ptr<arma::vec> PVEC;
#endif


#endif //TYPEDEFS_HAIOEMVUEk65df6df