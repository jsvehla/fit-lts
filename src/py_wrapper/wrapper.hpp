#ifndef _FITLTS_WRAPPER_HPP_SKDFHLDKASLLKDSN
#define _FITLTS_WRAPPER_HPP_SKDFHLDKASLLKDSN

/* Cmake will define ${LIBRARY_NAME}_EXPORTS on Windows when it
configures to build a shared library. If you are going to use
another build system on windows or create the visual studio
projects by hand you need to define ${LIBRARY_NAME}_EXPORTS when
building a DLL on windows.
*/
// We are using the Visual Studio Compiler and building Shared libraries

#if defined (_WIN32)
	#if defined(fitltswrapper_EXPORTS)
		#define DLLEXPORT __declspec(dllexport)
	#else
		#define DLLEXPORT __declspec(dllimport)
	#endif
#else
	#define DLLEXPORT
#endif

#if !defined( SWIG )
	#include <iostream>
	#include <vector>
	#include <armadillo>
	#include <boost/shared_ptr.hpp>
	#include "LTSAlgorithmVA.h"	
	#include "LTSAlgorithmBSA.h"	
	#include "LTSAlgorithmBSAM.h"
#endif

#pragma warning(disable:4251)

class DLLEXPORT FITLTSWrapper
 {

public:
	FITLTSWrapper( arma::mat& matrix ) {
		disableExceptionPrint();
		
		// easier to split matrix in arma that numpy - do it here
		m_Resp = matrix.col(matrix.n_cols - 1);
		matrix.shed_col(matrix.n_cols - 1);
		m_Expl = matrix;
		
		#ifdef DEBUG
		matrix.raw_print();		
		std::cout << std::endl;
		m_Expl.raw_print();
		m_Resp.t().raw_print();
		#endif
	};
	
	~FITLTSWrapper() {
		delete nulls;
	}
	
	void solveVA(int cycles, int kParam, int hParam) {
		LTSAlgorithmVA va = LTSAlgorithmVA(kParam, hParam, cycles, m_Expl, m_Resp);
		
		LTSResult result;
		va.solve(result);
		m_Results.push_back(result);
	}
	
	void solveBSA(int hParam) {
		BSACache* cache = new BSACacheNull();
		LTSAlgorithmBSA bsa = LTSAlgorithmBSA(hParam, m_Expl, m_Resp, *cache);
		
		LTSResult result;
		bsa.solve(result);
		m_Results.push_back(result);
				
		delete cache;
	}
	
	void solveBSAM(int lowHParam, int highHParam){
		BSACache* cache = new BSACacheNull();
		LTSAlgorithmBSAM bsam = LTSAlgorithmBSAM(lowHParam, highHParam, m_Expl, m_Resp, *cache);
		
		LTSResult result;
		bsam.solve(result);
		bsam.getResultsByH( m_Results );
				
		delete cache;
	}
	
	arma::mat getResultVectors(){
		//no uvec mapping?
		arma::mat vmat;
		for (std::vector<LTSResult>::iterator it = m_Results.begin() ; it != m_Results.end(); ++it) {
			arma::vec v = arma::conv_to<arma::vec>::from((*it).getVector());
			vmat = arma::join_cols(vmat, v.t());
			#ifdef DEBUG
			vmat.raw_print();
			#endif
		}
		return vmat;
	}
	
	arma::mat getResultEstimates(){
		arma::mat emat;
		for (std::vector<LTSResult>::iterator it = m_Results.begin() ; it != m_Results.end(); ++it) {
			emat = arma::join_cols(emat, (*it).getOLSEstimate().t());
			#ifdef DEBUG
			emat.raw_print();
			#endif
		}
		return emat;
	}
	
	arma::mat getResultResiduals(){
		std::vector<double> residuals;
		for (std::vector<LTSResult>::iterator it = m_Results.begin() ; it != m_Results.end(); ++it) {
			residuals.push_back((*it).getResidualsSum());
		}
		arma::mat rmat = arma::mat(residuals);
		return rmat;
	}
	
private:
	void disableExceptionPrint() {
		nulls = new std::ofstream(0);
		arma::set_stream_err1(*nulls);
		arma::set_stream_err2(*nulls);	
	}

	arma::mat m_Resp;
	arma::mat m_Expl;
	std::vector<LTSResult> m_Results;
	std::ofstream* nulls;
};

#endif
