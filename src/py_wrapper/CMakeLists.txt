# Copyright (C) 2012 thomas.natschlaeger@gmail.com
# 
# This file is part of the ArmaNpy library.
# It is provided without any warranty of fitness
# for any purpose. You can redistribute this file
# and/or modify it under the terms of the GNU
# Lesser General Public License (LGPL) as published
# by the Free Software Foundation, either version 3
# of the License or (at your option) any later version.
# (see http://www.opensource.org/licenses for more info)

cmake_minimum_required(VERSION 2.8.5 FATAL_ERROR)

project( FITLTS_Wrapper )

SET( CMAKE_VERBOSE_MAKEFILE ON )

### FITLTS
SET( FITLTS_PATH "{PROJECT_SOURCE_DIR}/../../../build/bin")
FIND_LIBRARY(FITLTS_LIBRARY
  NAMES libFIT-LTS-Lib.a
  PATHS ${FITLTS_PATH}
)
if(FITLTS_LIBRARY)
  message( STATUS "FITLTS")
  message( STATUS "  FITLTS link library is ${FITLTS_LIBRARY}")
else()
  message( STATUS "FITLTS NOT FOUND" )
endif()

### Armadillo
# SET( ARMA_PATH ".\\lib\\armadillo-3.4.4")
SET( ARMA_PATH "/usr/include")
find_path( ARMADILLO_INCLUDE_DIR
		NAMES armadillo
		PATHS ${ARMA_PATH}
		PATH_SUFFIXES "include"
)
message( STATUS "Armadillo" )
message( STATUS "  IncDirs: ${ARMADILLO_INCLUDE_DIR}" )

### LAPACK
# SET( LAPACK_NAMES "lapack_win32_MT")
# SET( LAPACK_PATH ".\\lib\\armadillo-3.4.4\\examples\\lib_win32")
SET( LAPACK_PATH /usr/lib)
SET( LAPACK_NAMES lapack)
FIND_LIBRARY(LAPACK_LIBRARY
  NAMES ${LAPACK_NAMES}
  PATHS ${LAPACK_PATH}
)
if(LAPACK_LIBRARY)
  message( STATUS "Lapack")
  message( STATUS "  Lapack link library is ${LAPACK_LIBRARY}")
else()
  message( STATUS "Lapack NOT FOUND" )
endif()

### BLAS
# SET( BLAS_PATH ".\\lib\\armadillo-3.4.4\\examples\\lib_win32")
# SET( BLAS_NAMES "blas_win32_MT")
SET( BLAS_PATH /usr/lib)
SET( BLAS_NAMES blas)
FIND_LIBRARY(BLAS_LIBRARY
  NAMES ${BLAS_NAMES}
  PATHS ${BLAS_PATH}
)
if(BLAS_LIBRARY)
  message( STATUS "BLAS")
  message( STATUS "  BLAS link library is ${BLAS_LIBRARY}")
else()
  message( STATUS "BLAS NOT FOUND" )
endif()

### Swig
find_package(SWIG REQUIRED)
include( ${SWIG_USE_FILE} )
message( STATUS "SWIG ${SWIG_VERSION}" )
message( STATUS "  Executable: ${SWIG_EXECUTABLE}" )

### Python
find_package( PythonLibs REQUIRED )
find_package( PythonInterp REQUIRED )
message( STATUS "Python ${PYTHON_VERSION_STRING}" )
message( STATUS "  Executable: ${PYTHON_EXECUTABLE}" )

### Boost
#SET(BOOST_ROOT "C:\\libs\\boost_1_53_0")
find_package( Boost 1.42.0 REQUIRED )
message( STATUS "Boost C++" )
message( STATUS "    IncDirs: ${Boost_INCLUDE_DIRS}" )


### NumPy
if( NOT NUMPY_INCLUDE_DIRS ) 

	execute_process(COMMAND ${PYTHON_EXECUTABLE} -c "import numpy; print numpy.get_include();"
					  RESULT_VARIABLE NUMPY_RESULT
					  OUTPUT_VARIABLE NUMPY_OUTPUT
					  ERROR_VARIABLE NUMPY_ERRUR
					  )

	if( ${NUMPY_RESULT} )
		message( FATAL "Failed to find numpy includes" )
	else()
		string(STRIP ${NUMPY_OUTPUT} NUMPY_INCLUDE_DIRS)
		set( NUMPY_INCLUDE_DIRS ${NUMPY_INCLUDE_DIRS} CACHE PATH "Include path for numpy headers" )
		
		execute_process(COMMAND ${PYTHON_EXECUTABLE} -c "import numpy; print numpy.version.full_version;"
					  RESULT_VARIABLE NUMPY_RESULT
					  OUTPUT_VARIABLE NUMPY_OUTPUT
					  ERROR_VARIABLE NUMPY_ERRUR
					  )

		string(STRIP ${NUMPY_OUTPUT} NUMPY_VERSION_STRING)
		set( NUMPY_VERSION_STRING ${NUMPY_VERSION_STRING} CACHE STRING "Numpy version" )
	endif()
	
	
endif()
message( STATUS "Numpy ${NUMPY_VERSION_STRING}" )
message( STATUS "  Version: ${NUMPY_VERSION_STRING}" )
message( STATUS "  Include: ${NUMPY_INCLUDE_DIRS}" )

if( MSVC )
	# for multi-config build MSVC
	foreach( OUTPUTCONFIG ${CMAKE_CONFIGURATION_TYPES} )
		string( TOUPPER ${OUTPUTCONFIG} OUTPUTCONFIG )
		set( CMAKE_RUNTIME_OUTPUT_DIRECTORY_${OUTPUTCONFIG} "${PROJECT_BINARY_DIR}/bin" )
		set( CMAKE_LIBRARY_OUTPUT_DIRECTORY_${OUTPUTCONFIG} "${PROJECT_BINARY_DIR}/bin" )
		set( CMAKE_ARCHIVE_OUTPUT_DIRECTORY_${OUTPUTCONFIG} "${PROJECT_BINARY_DIR}/bin" )
	endforeach()
else()
	# For the generic no-config case (e.g. with mingw)
	set( CMAKE_RUNTIME_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/bin" )
	set( CMAKE_LIBRARY_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/bin" )
	set( CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/bin" )
endif()

set( ARMANPY_INCLUDE_DIR ${PROJECT_SOURCE_DIR}\\..\\..\\lib\\armanpy-0.1.1\\include )
include_directories(
	${PROJECT_SOURCE_DIR}
	${ARMANPY_INCLUDE_DIR}
	${NUMPY_INCLUDE_DIRS}
	${PYTHON_INCLUDE_DIRS}
	${CMAKE_CURRENT_SOURCE_DIR}
	${Boost_INCLUDE_DIRS}
	${ARMADILLO_INCLUDE_DIR}
	"..\\LTS"
)

add_library( fitltswrapper SHARED wrapper.cpp wrapper.hpp)
target_link_libraries(fitltswrapper  ${FITLTS_LIBRARY} ${BLAS_LIBRARY} ${LAPACK_LIBRARY})

SET( CMAKE_SWIG_OUTDIR "${PROJECT_BINARY_DIR}/bin" )
set_source_files_properties( wrapper.i PROPERTIES CPLUSPLUS ON)
set_source_files_properties( wrapper.i PROPERTIES SWIG_FLAGS "-includeall;-ignoremissing" ) # ;-debug-tmused;-debug-typedef;-debug-typemap" )
swig_add_module( fitltswrapper python wrapper.i )
swig_link_libraries( fitltswrapper fitltswrapper ${PYTHON_LIBRARIES} )
set( SWIG_MODULE_armanpytest_EXTRA_DEPS armanpy.i numpy.i wrapper.cpp wrapper.hpp )
