# Copyright (C) 2012 thomas.natschlaeger@gmail.com
# 
# This file is part of the ArmaNpy library.
# It is provided without any warranty of fitness
# for any purpose. You can redistribute this file
# and/or modify it under the terms of the GNU
# Lesser General Public License (LGPL) as published
# by the Free Software Foundation, either version 3
# of the License or (at your option) any later version.
# (see http://www.opensource.org/licenses for more info)

import os, sys, warnings
import numpy as N

sys.path.append( "./build/bin" )
from fitltswrapper import *

def example_usage():

	mat = N.array([
	[1.,80.,27.,89.,42.],
	[1.,80.,27.,88. ,37.],
	[1.,75.,25.,90. ,37.],
	[1.,62.,24.,87. ,28.],
	[1.,62.,22.,87. ,18.],
	[1.,62.,23.,87. ,18.],
	[1.,62.,24.,93. ,19.],
	[1.,62.,24.,93. ,20.],
	[1.,58.,23.,87. ,15.],
	[1.,58.,18.,80. ,14.],
	[1.,58.,18.,89. ,14.],
	[1.,58.,17.,88. ,13.],
	[1.,58.,18.,82. ,11.],
	[1.,58.,19.,93. ,12.],
	[1.,50.,18.,89. ,8.],
	[1.,50.,18.,86. ,7.],
	[1.,50.,19.,72. ,8.],
	[1.,50.,19.,79. ,8.],
	[1.,50.,20.,80. ,9.],
	[1.,56.,20.,82. ,15.],
	[1.,70.,20.,91. ,15.]], order="F" )
	
	ex = FITLTSWrapper( mat )
	#ex.solveVA(5000, len(mat[0]), 12 );
	ex.solveBSAM( 10, 13 );
	print(ex.getResultResiduals())
	print(ex.getResultVectors())
	print(ex.getResultEstimates())	
	print( "Expected result:\n-35.2095 0.746057 0.337795 -0.0054919" )
 
 
	
	
 
if __name__ == '__main__':
 example_usage()
