\chapter{Implementation}\label{c:implementation}

\section{Platform}
The implementation platform for the core library (specified in the requirements) is C++. To avoid the programming complexity of web application written in C++, as the implementation framework for the frontend Python framework Django\footnote{\url{https://www.djangoproject.com/}} was chosen.

Django framework provides required features for website development, including ORM\footnote{Object-relational mapping.} features and is widely supported by the community and web programmers. It also provides extensible HTML template system and easy to work with admin module, allowing to review and change database data without directly accessing a database. Together with settings module, which easily allows to configure various aspects of Django framework and its associated tools, the configurability covers the application requirements. Furthermore, using Python based framework enables easy interaction with the C++ code.

Development platform for core library was initially MS Windows, but the care was taken to allow for multi-platform design. Web application was developed on Linux (Ubuntu-based distribution) because Django generally works better in Linux environment and documentation for its various functionality (such as scheduled tasks) is more readily available for Linux deployment servers. The core library is thus proven to work on both platforms.

\section{Core library}
As previously mentioned, the core library is developed in C++. Since C++ doesn't provide higher algebraic functionality by default, a library to cover the required computations was needed. The chosen library is Armadillo\footnote{\url{http://arma.sourceforge.net/}}, version 3.4.4.

This Armadillo library is implemented as a wrapper and extension to the (originally Fortran) libraries BLAS\footnote{\url{http://www.netlib.org/blas/}} and LAPACK\footnote{\url{http://www.netlib.org/lapack/}} and it allows to use any library which implements the same interface. The deployed application can benefit from this by using one of many optimized implementation of these libraries.

The further benefit of this library is delayed evaluation feature which helps to optimize memory usage and speed which is needed for the LTS computations.

The core library project also provides simple command line interface. Parsing of command line arguments is handled by the library TCLAP\footnote{\url{http://tclap.sourceforge.net/}}.

\section{SWIG and C++ wrapper}
To allow interaction between C++ and Python code, it was needed to write a C++ wrapper. To avoid unnecessary complexity of developing wrapper by hand, SWIG\footnote{\url{http://www.swig.org/}} was used. SWIG is a wrapper/interface generator capable of reading C++ code according to the so called interface files to generate appropriate wrapper.

To implement the FITLTS wrapper, the ArmaNpy wrapper files\footnote{\url{http://sourceforge.net/projects/armanpy/}} linked from the Armadillo home page were used. The already presented solution was used instead of developing same functionality which would be potentially error-prone and delayed development. These interface files do not implement full functionality of Armadillo, but at least provide wrapper objects to the core classes of Armadillo \texttt{arma::mat<double>} by directly using Python library NumPy\footnote{\url{http://www.numpy.org/}} as an interface to Python side of the code.

The drawback of using the wrapper and ArmaNpy is that the developer (caller) must take care to avoid errors caused by memory ownership when one side cannot modify others' memory. The example of such situation and the solution from the example files provided by ArmaNpy is:

\begin{Verbatim}
# In the following the size and content of a matrix
# is modified
m = N.array( [ [1.,2.,3.], [4.,5.,0.6] ], order="F" )
ex.modify( m, 9, 9 )
	
# Note that after the matrix was modified by this function
# it does not own its memory. So one can e.g. not resize it
m.resize( 81, 1 )
ValueError: cannot resize this array: it does not own its data

# But it is easy to copy it and work with it
m = N.array( m )
m.resize(81,1)
\end{Verbatim}

\subsection{Wrapper design}
Wrapper was designed to allow very simple interaction. It fully covers the needed calls to set algorithm parameters and get a result. After the computation the results must be constructed by a caller using \texttt{getResult*} methods. In the case of the BSAM algorithm, these matrices represent a list of results where each row represents single result sorted by the parameter $h$. This approach was chosen for its simplicity and clearly defined data-flow.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.6\textwidth]{img/CD_Wrapper.eps}
	\caption{Class diagram for wrapper.}
	\label{fig:diag:wrapper}
\end{figure}

\section{Folder structure}
\subsection{C++ library project}
The C++ project follows a simple structure, the project separates required libraries, source code and test cases into separate folders. Source code folder further separates library sources and wrapper sources.
\vspace{0.5em}
\dirtree{%
.1 bin				\DTcomment{Compiled binaries (Win)}.
.1 doxygen			\DTcomment{Developer documentation}.
.2 doxygen			.
.2 html				.
.2 latex			.
.2 Doxyfile			\DTcomment{Developer documentation}.
.1 lib				\DTcomment{Required libraries}.
.1 src.
.2 LTS				\DTcomment{Sources for FITLTS C++ library}.
.2 py\_wrapper		\DTcomment{Sources and build files for wrapper}.
.2 main.cpp			\DTcomment{Simple CL interface source}.
.1 test.
.2 data				\DTcomment{Original data sets for testing}.
.2 FacadeGTest.cpp	\DTcomment{Various supporting code for test cases}.
.2 FacadeGTest.h.
.2 test\_BSA.cpp	\DTcomment{BSA test cases}.
.2 test\_BSAM.cpp	\DTcomment{BSAM test cases}.
.2 test\_VA.cpp		\DTcomment{VA test cases}.
.1 VS2010			\DTcomment{Visual studio project files}.
.1 LICENSE.
.1 README.
.1 CMakeList		\DTcomment{Library+CLI build script}.
}\vspace{0.5em}

The source code is commented, all implementation details and class overview can be easily reviewed by reading doxygen generated documentation.

\subsection{Django web application project}
Django project is normally separated into a ``site'' root folder and various sub-projects folders. These folders are referred to by Python standards as modules. Each Python module is either a file or folder with file \texttt{\_\_init.py\_\_} inside. This allows for including all classes, functions and like into the program by using Python \texttt{import} command. The frontend project follows this philosophy and organizes the default Django sub-project structure in the following way:
\vspace{0.5em}
\dirtree{%
.1 admin			\DTcomment{Sources for the customizing of the admin interface}.
.1 models			\DTcomment{Model sources}.
.1 templates		\DTcomment{Django templates files}.
.1 tests			\DTcomment{Source code of test cases}.
.1 views			\DTcomment{View source files}.
.1 urls.py			\DTcomment{URL mapping}.
.1 \_\_init.py\_\_.
}
\vspace{0.5em}

By default, \texttt{admin}, \texttt{models}, \texttt{tests} and \texttt{views} are each represented as a single file. To organize sub-project better, the file modules were replaced by folder modules with appropriate names. This doesn't affect logical view of a Python application as long as the folder include the init file to signify it's a module.

This structure is default for all FITLTS sub-projects, but some of them might tweak the structure by adding additional modules or excluding the unneeded ones. Any specific changes will be explicitly mentioned.

It's also important to note that the application uses some external modules (such as pdf exporting) to simplify and speed up development. Modules and how to install runtime requirements are listed in Chapter~\ref{c:deployment}.

\vspace{0.5em}
\noindent The site project folder consists of the sub-project in the following way:
\dirtree{%
.1 DynamicFormWizard		\DTcomment{Module for generic form wizard}.
.2 wizards					\DTcomment{Source code}.
.2 \_\_init\_\_.py.
%
.1 FITLTS					\DTcomment{Project main folder}.
.2 settings					\DTcomment{Application settings files}.
.2 templates				\DTcomment{Templates not associated with any sub-project}.
.2 urls.py					\DTcomment{Global URL mapping}.
.2 wsgi.py					\DTcomment{WSGI file for HTTP server}.
.2 \_\_init\_\_.py.
%
.1 LTSAlgorithms			\DTcomment{Computation module}.
.2 wrapper					\DTcomment{C++ Wrapper files}.
.3 \_\_init\_\_.py				
.3 \_fitltswrapper.so
.3 fitltswrapper.py
.3 fitltswrapper.so
.2 ....
%
.1 LTSSummary				\DTcomment{Summary output module}.
%
.1 LTSTasks					\DTcomment{Tasks module}.
.2 wizard					\DTcomment{Specification of form wizard}.
.2 forms					\DTcomment{Forms for data input}.
.2 \_\_init\_\_.py.
.2 ....
%
.1 QueueCelery				\DTcomment{Celery tasks}.
%
.1 LTSFactories				\DTcomment{Module for factories}.
.2 \_\_init\_\_.py.
.2 loader.py.
.2 ....
%
.1 SQLiteDB					\DTcomment{Database files}.
.1 static					\DTcomment{static web files}.
.1 manage.py.
}
\vspace{0.5em}

\section{Django application sub-projects}
In the previous chapter, we have described the folder structure of the frontend project. In this chapter we will talk about each sub-project and mention any special cases or note things that can be described as not common implementation problems.

\subsection{FITLTS}
Each Django application has a main folder, which serves as an entry point to the application. The folder also includes settings for various aspects of Django, Celery and other configurations. By developer's decision, this folder also includes \texttt{templates} sub-folder which holds all templates that are not directly associated to any sub-project functionality. For example the base template, index page, algorithms and library description page.

\subsection{LTSAlgorithms}
The sub-project consists of classes related to the computation of LTS estimates. Includes model classes for matrix (Figure~\ref{fig:diag:ltsmat}), algorithms (Figure~\ref{fig:diag:ltsalg}) and transformations (Figure~\ref{fig:diag:ltstrans})\footnote{Keep in mind that Python is not strongly typed language, the type specification in the UML diagrams means methods accept any object implementing  same interface.}.

\begin{figure}[htb]
	\centering
	\includegraphics[width=\textwidth]{img/CD_LTSAlgorithm.eps}
	\caption{Class diagram--Algorithms.}
	\label{fig:diag:ltsalg}
\end{figure}

The classes responsible for computation are subtypes of \texttt{LTSAlgorithm\allowbreak Base}. When the subtypes of a class is retrieved from a database, a retrieved object has an incorrect type of it's parent. This is unwanted behaviour caused by relation database and doesn't allow to follow OOP principles easily. To avoid this, \texttt{LTSAlgorithmBase} overrides Django \texttt{save} method and saves a runtime type of the subclass instance to the database. After the retrieval, the method \texttt{as\_leaf\_class} can be called to get the object with correct type and parameters. The same process is used when saving \texttt{LTSTransBase} subtypes.

Additionally, both \texttt{LTSTransBase} and \texttt{LTSAlgorithmBase} implements a Visitor pattern (see method \texttt{accept}) mainly used here to separate view from models. Python doesn't provide method overloading, which complicates the implementation of visitor. To solve this issue a visitor helper must be implemented~\cite{PythonVisitor}. The visitor helper implementation uses custom annotations and dispatcher, which decides which visitor method to call based on runtime type and method annotation. The implemented visitors are \texttt{ParamVisitor}, \texttt{TransFormParamVisitor} responsible for providing readable overview to the user and \texttt{TransParamVisitor} to translate transformation parameters into format of UI forms.

\begin{figure}[htb]
	\centering
	\includegraphics[width=\textwidth]{img/CD_LTSMatrix.eps}
	\caption{Class diagram--Matrix.}
	\label{fig:diag:ltsmat}
\end{figure}

\texttt{LTSMatrix} uses class \texttt{LTSRow} to save its data. The data are saved as a base64 encoded string using custom Django field \texttt{Base64Array} which handles all required encoding and decoding.

\begin{figure}[htb]
	\centering
	\includegraphics[width=\textwidth]{img/CD_LTSTransformations.eps}
	\caption{Class diagram--Transformations.}
	\label{fig:diag:ltstrans}
\end{figure}

\subsection{DynamicFormWizard}
During the analysis of the implementation platform (Django), it became obvious it doesn't support creation of form wizards as requested in requirements. The aim of this module is to provide skeleton that can easily be inherited and implemented to create dynamically changing wizard based on the input data.

\begin{figure}[htb]
	\centering
	\includegraphics[width=\textwidth]{img/CD_Wizard.eps}
	\caption{Class diagram--DynamicWizard.}
	\label{fig:diag:wizard}
\end{figure}

The basic idea is to let the class \texttt{DynamicWizard} encapsulate form handling (see~\cite{DjangoForms}), temporary data storage and transitions between each form. While \texttt{DynamicWizard} handles the overall functionality, \texttt{WizardStep} class' interface is directly tied to the form (such as submitting). 

After the user access url on which wizard listens, the wizard handles requests using \texttt{handle\_request} method. This method checks the step\_id, which is passed to it by Django URL dispatcher~\cite{DjangoUrls}. For this to work properly, the wizard's url must include the regexp pattern for step\_id \verb|(?P<step_id>[A-Za-z0-9_-]+)|. The input data are temporarily stored in session storage as a class \texttt{WizardState}. The wizard is implemented to load data from session storage to fill forms an active user has previously submitted. This way users will not lose any submitted data even when they returns to previous steps. This also allows to fill forms with (initial) data before users even visits it, an example usage of this can be seen during task duplication (Section. \ref{sec:ltstask}).

This method then calls \texttt{handle\_GET}, \texttt{handle\_POST}, \texttt{handle\_CANCEL} or \texttt{handle\_DONE} depending on the HTTP method, url in the request or the number of the steps.

Calling GET method means the form was requested by the user. This results in sequence of calls \texttt{get\_form\_data} which retrieves the past data (or none) from session storage, \texttt{initialize\_form} on class \texttt{WizardStep} and last \texttt{\_render\_form}. This method will then return a HTTPResponse object with additional context data created by \texttt{pre\_render\_form} of current \texttt{WizardStep} instance.

POST is called when the user submits data to the form. The wizards delegates the post request to the step instance (method \texttt{post\_to\_form} to let step handle any specifics for given step. Afterwards, the form is checked if it's valid, if it's not the user is notified with error messages, otherwise the wizard save the data into session storage and redirects the user to the next step.

When there are no more steps, the method \texttt{handle\_DONE} is called. Anytime, user can also cancel filling of the form. This is handled by accessing cancel step with name specified by \texttt{get\_cancel\_step}. The cancel cleans session storage by default, but can be overridden to provide any functionality developers wants.

As mentioned previously, this module provides skeleton functionality. The specific steps and forms must be implemented by a developer. This module is built in a way that the wizard must be inherited and methods \texttt{get\_steps\_list}, \texttt{handle\_DONE} of \texttt{DynamicWizard} and \texttt{get\_template} of \texttt{WizardStep} must be implemented before using the wizard. These methods are not tied to any specific implementation style, but example implementation using subtype for each specific step can be seen in module \texttt{LTSTasks.wizard}. This implementation also shows how to handle passing data between each steps using session storage and how to create forms with dynamic number of fields. The specific ordering of these steps is handled in \texttt{get\_steps\_list} by reading current state from session storage and adding appropriate steps.


\subsection{LTSTasks}\label{sec:ltstask}
The module is responsible for grouping the user input data into tasks and task groups. The module also includes submodules for specification of wizard as mentioned previously and classes for data input forms. The class diagram for model classes can be seen in Figure~\ref{fig:diag:ltstasks}.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.9\textwidth]{img/CD_LTSTasks.eps}
	\caption{Class diagram--LTSTasks.}
	\label{fig:diag:ltstasks}
\end{figure}

Similarly to the algorithm module, the \texttt{LTSTask} stores the state using custom Django field. The state is stored into the database as a two characters string specified by \texttt{get\_char\_name} method. \texttt{TaskStateField} is responsible for translation between the object and database identifier using the class \texttt{DBChoiceFactory} which works identically to \texttt{ObjectChoiceFactory} in algorithm module.

The module has two implemented wizards. \texttt{WizardNew} serves to implement steps needed for creating new tasks. \texttt{WizardEdit} is its subtype and adds method to fill session data from database and replaces first step. Task editing functionality is implemented as task duplication, to do that, the \texttt{WizardEdit} has a method \texttt{prepare\_data}. As mentioned this method fill the empty session storage with data loaded from database. This effectively means the edit wizard creates edited duplicate of the task after confirm.

\begin{samepage}
\noindent The module includes these forms:
\begin{description}\itemsep1pt \parskip0pt \parsep0pt
	\item[UserDetailsForm]		Matrix upload and user email input.
	\item[MailDetailsForm]		User email only input.
	\item[MatrixDetailsForm]	Specification of matrix transformation. Dynamic number of select boxes.
	\item[AlgorithmForm]		Specification of used algorithms. At leas one must be not none.
	\item[AlgorithmVADetailsForm]	Model form. Parameters of VA.
	\item[AlgorithmBSADetailsForm]	Model form. Parameters of BSA.
	\item[AlgorithmBSAMDetailsForm] Model form. Parameters of BSAM.
	\item[ConfirmForm]				Empty form. Serves only to avoid null error in confirm step.
\end{description}
\end{samepage}

\subsection{LTSFactories}
\begin{figure}[htb]
	\centering
	\includegraphics[width=\textwidth]{img/CD_ChoiceFactory.eps}
	\caption{Class diagram--Object factory.}
	\label{fig:diag:objectchoice}
\end{figure}

To allow easy extension of application and easy interaction of UI and model, the factory module implements classes (in Figure~\ref{fig:diag:objectchoice}) which are able to create instances from object names and parameters. This is done using dictionary (string, class) with class name as a key. Adding classes to the factories is done by registering them using \texttt{register\_class} method and retrieving them by calling \texttt{get\_object} method.

\texttt{ObjectChoiceFactory} in Figure \ref{fig:diag:objectchoice}, serves to translate strings from select boxes and input fields from UI into objects with given parameters. The developer has to register class to the factory and any registered class must implement static method \texttt{get\_long\_name}. This method then serves to provide a name shown in the UI to the user. This class is used in application to handle \texttt{LTSTransBase} subtypes. For \texttt{LTSAlgorithmBase} subtypes the class \texttt{AlgDetailsFactory} adds association between details form and algorithm class all other calls are delegated to the internal \texttt{ObjectChoiceFactory} instance.

Interesting method is \texttt{get\_object} with \texttt{*args, **kwargs} parameters, which accepts named parameters and directly call the class constructor with them. In case where there are some extra named parameters, the error is raised. This behavior is expected and wanted:

\begin{Verbatim}
factory.get_object( LTSAlgorithmVA.__name__,
	cycles=10, kParam=5, hParam=2 )
\end{Verbatim}
creates an instance of VA algorithm with given parameters, while:
\begin{Verbatim}
factory.get_object( LTSAlgorithmVA.__name__,
	cycles=10, kParam=5, hParam=2, lowHParam=1 )
\end{Verbatim}
is not valid call.
Combined with Python's ability to unpack dictionaries (UI forms provide dictionaries of input values), this implementation allows for directly passing algorithms' parameter data between UI and model without complex translations.

\texttt{DBChoiceFactory} is a specialization to translate between 2 character flags from database to object instances (used for \texttt{TaskState}). Instead of \texttt{get\_}\texttt{long\_}\texttt{name}, classes mus implement \texttt{get}\texttt{\_char}\texttt{\_name} which will be saved to the database.

\subsection{LTSSummary}
LTSSummary sub-project serves to provide functionality needed to review parameters and result. The module is implemented as a Django view that loads the requested task group from the database and then pass it to renderer which reads a required data, process them into context dictionary and returns a HTTP response, which is then returned to the user. \texttt{RendererBase} in Figure~\ref{fig:diag:ltssummary} serves as a parent to all renderers, the method responsible for returning HTTPResponse is \texttt{render\_response} which is abstract and in the case of \texttt{RendererSimple} and  \texttt{RendererCompare} simply fills templates with generated context and return it to the view. In the case of \texttt{RendererPDF}, the response is created by rendering Django template, converting it to PDF format and sending the resulting file to the user.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.7\textwidth]{img/CD_LTSSummary.eps}
	\caption{Class diagram--LTSSummary.}
	\label{fig:diag:ltssummary}
\end{figure}

\subsection{QueueCelery}
The module responsible for Celery tasks. It includes only two tasks so far: \texttt{ComputeTask} and \texttt{MailTask}. Compute task calls appropriate method on \texttt{Task} instances to compute the results and mail task sends confirm mails to the user.

To incorporate easy editing of mail message, the templated mail library is used. This allows the developer to change the message without needing to edit code. The template can be found in \texttt{LTSTasks/templates/templated\_} \texttt{email} folder. The template has a single parameter \verb|{{ url }}| which is replaced by url to the summary page for submitted task.

\section{Extending the application}\label{sec:extending}
This section deals with basics of extending of the application functionality---adding new algorithms and matrix transformations. Care was taken to make this as automatic as possible

\subsection{Adding algorithms}
\begin{enumerate}[label=\arabic*.]
\item \textbf{Implement LTSAlgorithmBase subtype}\hfill\\ Do this either directly in Python or using wrapper to another library. Do not forget to implement all abstract methods.
\item \textbf{Implement the form for algorithm parameters.}\hfill\\ This can be done by using Django's internal \texttt{ModelForm} class, see \texttt{LTSTasks.}\texttt{forms} how current forms are implemented. Do not forget to implement validation methods. The model integrity restriction are only checked when the model is saved to database. Input data are saved temporarily into session storage and only way to inform user about errors is to correctly implement validations on forms.
\item \textbf{Add algorithm and associated form to the settings.}\hfill\\ Add a tuple (algorithm class, form class) to \texttt{ALGORITHM\_CLS} using full module path. After the application is loaded, the global factories are instantiated using these settings. The factory will take care of generating choices in the choose algorithms step (Figure~\ref{fig:diag:webproc:taskinput}) and appropriate forms in detail steps.
\end{enumerate}

\subsection{Adding transformations}
\begin{enumerate}[label=\arabic*.]
\item \textbf{Implement LTSTransBase subtype.}
\item \textbf{Add validation for parameter to the form.}\hfill\\ Add a validation method with name \texttt{clean\_param\_<transformation class name>} to the \texttt{MatrixDetailsForm} which returns a dictionary of constructor parameters. For example if the new transformation takes parameters \texttt{multiply} and \texttt{base} return a dictionary
\begin{Verbatim}
{ "multiply": value, "base": value2 }.
\end{Verbatim} 
\item \textbf{Register implemented transformation to application.}\hfill\\ Add a module path to the class to \texttt{TRANSFORMATION\_CLS}. Again, the underlying factory will take care of providing choices and translation between UI and model.
\end{enumerate}


\section{Optimizations}\label{sec:optimizations}
During the development a few optimizations were done to make computation faster, some minor, such as to avoid allocating new memory needlessly, some more significant that introduced changes to the algorithm pseudocode. A Significant one was to the computation of OLS estimate for vector $w$. Using \eqref{eq:linreq:j}, the way to compute the OLS estimate for vector $w$ is:
\begin{equation}
J\left(w\right) = \left(X^{T} diag\left(w\right) X\right)^{-1}X^{T} diag\left(w\right) Y.
\end{equation}

\noindent The computational complexity of this method is:
\begin{multline}
O\left(J\left(w\right)\right) = O\left(np\right) + O\left(n^2p\right) + O\left(np^2\right) + O\left(p^3\right) + O\left(np\right) + O\left(np^2\right) + \\ + O\left(n^2p\right) + O\left(np\right) = 3O\left(np\right) + O\left(p^3\right) + 2O\left(n^2p\right) + 2O\left(np^2\right) = \\ = O\left(n^2p\right)
\end{multline}

\noindent Now, if we lessen the number of rows from $n$ to $h$ before the computation instead of using $diag\left(w\right)$ the computation becomes:
\begin{multline}
O\left(J^{'}\left(w\right)\right) = 3O\left(hp\right) + O\left(p^3\right) + 2O\left(hp^2\right) = O\left(p^2h\right)
\end{multline}

Since the removing of rows can be done in $O\left(n\right)$ complexity, we can clearly see that the second equation is lesser that first.

Second experiment with optimizations was introduction of caching to prevent computing same estimates multiple times. To implement cache, a hash table with key vector and value estimate was chosen. The map has lookup complexity of $O\left(1\right)$ at average or $O\left(n\right)$ at worst when all buckets become full. In our case, the amount of keys that needs to be stored is $\binom{n}{h}$ which causes for map for bigger data to be slower or of the sane speed as recomputing the estimates. Together with the expanded memory usage\footnote{Key uses only as much as it needs to represent vector in binary up to the size of \texttt{size\_t}.} which can eventually cause even more slowdowns on systems with the small amount of memory, we can come to the conclusion that the cache should be used carefully depending on the size of the data and HW specifications.

The measured time of BSA with and without time can be seen in Table~\ref{table:bsacache}. Datasets are explained in Apendix~\ref{app:datasets}The times were measured using googletest on PC with 4GB RAM and i5-2430M CPU and precompiled LAPACK and BLAS libraries included with Armadillo installation. The absolute times might differ depending on the HW configuration or the implementation of BLAS and LAPACK.

\begin{figure}[htb]
	\centering
	\begin{tabular}{| l | r | r |}
	\hline
	Dataset	& time (no cache) [ms]	& time (cache) [ms] \\ \hline\hline
	D1		& 220162				& 223818	\\
	D2 		& 2145					& 2180		\\
	D3 		& 2165					& 2395		\\
	D4 		& 34630					& 35672		\\
	D5		& 5						& <1		\\
	D6 		& 11465					& 11688		\\
	D7 		& 379857				& 393841	\\
	\hline
	\end{tabular}
	\caption{Comparison of BSA times with and without a cache.}
	\label{table:bsacache}
\end{figure}
