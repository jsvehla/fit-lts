\chapter{Design and analysis}\label{c:design}

\section{Vision}
The goal of this work is to create a library implementing the BSA and BSAM regression algorithms as defined in Section \ref{c:alg:bsa} and a showcase web application with an algorithm overview and ability to apply them on data sets input by a user.

\section{Overview}
The application consists of two parts: backend (also called core) library and frontend web application. Frontend consist of a website user interface to allow users quick experimentation and computation of estimates with various parameters. The user will specify the parameters in this interface and the business logic will then use backend library to provide user with the result. UI will also support comparison of algorithms and comprehensive result summaries. Backend consists of the core library with computational logic. The core library should be programmed in reusable way to allow offline use. Both frontend and backend should be extensible and programmed with good application design in mind.

\section{Requirements}\label{sec:requirements}
In this section we will describe application requirements. We'll divide the list into functional and nonfunctional requirements.

\subsection{Functional requirements}
\begin{enumerate}[label=FR-\arabic*]
\item Backend will implement multiple algorithms;
	\begin{enumerate}[label*=-\arabic*, topsep=0pt]
		\item \textit{VA algorithm} for randomized estimate.
		\item \textit{BSA algorithm} for exact estimate.
		\item \textit{Extended BSA algorithm} for computing estimate for multiple $h$ parameters.
	\end{enumerate}
\item Algorithms will be parametrized;
	\begin{enumerate}[label*=-\arabic*, topsep=0pt]\label{req:list:param}
		\item\label{req:params} Common parameters are: matrix of explanatory variables, matrix of response variable, number of not-trimmed measurements, presence of an intercept.
		\item Additional parameter for VA algorithms are number of cycles.
		\item Additional parameter for extended BSA is low and high bound for the number of not-trimmed measurements. These parameters override common not-trimmed parameter.
	\end{enumerate}
\item Backend result summary will include global minimum (the LTS estimate), the sum of residuals and the appropriate vector of response variables for the estimate.
\item Backend result summary for the BSAM will be a list of results for each number of not-trimmed measurements. The result summary will also indicate the lowest optimum of all sub-results.
\item Application will allow to compare results and computational time for multiple algorithms.
\item Frontend will implement computation in the form of tasks, the user will specify parameters in the web form wizard.
\item Frontend task will be queued and computed one after other depending on maximum number of worker threads int the queue.
\item Administrator will be able to specify the number of workers.
\item The task form will allow users to set algorithm and all appropriate parameters mentioned in \ref{req:list:param}.
\item\label{req:trans} Application will include means to edit input matrix data by specifying simple column transformations. More complex transformation will have to be done by the user before he  uploads data to the application. These transformations are:
	\begin{enumerate}[label*=-\arabic*, topsep=0pt]
		\item No transformation.
		\item Exclude column.
		\item Multiply column.
		\item Exponentiate column.
		\item Apply logarithm.
	\end{enumerate}
\item Tasks can be edited or cancelled after submitting. Edits after the worker started computing a task are not possible, such task can only be duplicated.
\item Finished tasks can be duplicated.
\item Users will receive summary of task parameters before submitting tasks to the queue.
\item Part of the web form will be user's email address. User will be mailed the link to the summary page after submitting the task.
\item The result page will include a task summary, current status and results. This page can be accessed anytime after a task is submitted.
\item\label{req:summarylink} The result page will only be accessible following the mailed link or link given after submitting the task.
\item Summary and result pages can be exported from website to offline formats, such as PDF.
\item Application will include administration interface.
\end{enumerate}

\subsection{Non-functional requirements}
\begin{enumerate}[label=FR-\arabic*]
\item Application will be separated into core library and web application.
\item Web part will use web framework for ease of implementation.
\item Core library will be implemented in C++ for greater portability and performance.
\item Website will include basic data and description of implemented processes and algorithms.
\item Core library will use C++ library for algebraic operations.
\item The application design will allow for easy addition of algorithms.
\end{enumerate}

\section{Computation Core Concept}
In application overview we mentioned that the application will consist of the core computation library and the web application. Concept diagram of the computation library can be seen in Figure~\ref{fig:diag:core}. Important part of the core is the abstract class \texttt{LTSAlgorithm} which serve as a base class for all implemented algorithms. By requirement \ref{req:params} a common parameter is the number of not-trimmed measurements (\texttt{hParam}). Subclasses represent algorithms VA, BSA and BSAM. Since BSA can be seen as a special case of BSAM when lower and higher bound of number of not-trimmed data points are equal, BSA can thus be designed by reusing the BSAM algorithm by using composition according to Composite Reuse Principle~\cite{knoernschild02}.

\begin{figure}[htb]
	\centering
	\includegraphics[width=\textwidth]{img/ConceptDiagramCore.eps}
	\caption{The concept diagram of the computation layer.}
	\label{fig:diag:core}
\end{figure}

The concept also shows class \texttt{BSACache} implemented as two subtypes \texttt{BSACacheMap} and \texttt{BSACacheNull}. Requirements for a cache is fast (less or equal $O\left(n\right)$, preferably constant) data retrieval so \texttt{BSACacheMap} implements a hash map functionality which provides required complexity. Class \texttt{BSACacheNull} is implemented as design pattern NullObject~\cite{DeLa98a} to avoid unnecessary checks if cache was set.

From BSA pseudocode, it's clear the algorithm iterates over $Q^{\left(n, p+1 \right)}$ combinations. This functionality is provided by class \texttt{Computations}. The process of generating sorted list of combinations is illustrated by pseudocode in Algorithm~\ref{alg:combinations}.

\begin{algorithm}
\caption{Generate all combinations.}
\label{alg:combinations}
\DontPrintSemicolon
\SetKwInOut{Input}{I}\SetKwInOut{Output}{O}
\Input{K number of outcomes, N number of possibilities}
\Output{Sorted combinations}
\BlankLine
\lFor{$i \leftarrow 1$ \KwTo $k$}{
	$s\left[i\right] \leftarrow i$
}
\For{$i \leftarrow 2$ \KwTo $C\left(n,k\right)$}{
	$m \leftarrow k$, $maxVal \leftarrow n$\;
	\While{$s\left[m\right] = maxVal$}{
		$m \leftarrow m-1$\;
		$maxVal \leftarrow maxVal-1$\;
	}
	$s\left[m\right] \leftarrow s\left[m\right]+1$\;
	\lFor{$j \leftarrow m+1$ \KwTo $k$}{
		$s\left[j\right] \leftarrow s\left[j-1\right]+1$\;
	}
	Add array $s$ to list of all combinations $Comb$\;
}
\Return $Comb$ as sorted list of all combinations
\end{algorithm}

\section{Frontend concept}
The frontend application (concept diagram in Figure~\ref{fig:diag:concept}) consists of the main classes \texttt{Task} and \texttt{TaskGroup}. \texttt{Task} serves as a holder of user-inputted data and provides unified interface for the computation. \texttt{TaskGroup} holds all tasks related to single data input. The typical scenario would be when single users requests a comparison of algorithms with different settings on a single set of data. 

An alternative design solution to this user scenario would be to define \texttt{Task} as an abstract parent to subtypes \texttt{TaskSimple} and \texttt{TaskCompare}, where \texttt{TaskCompare} would provide comparison functionality by being composite of multiple \texttt{TaskSimple} objects. After taking into consideration differences between relational database and OOP\footnote{Object Oriented Programming} design and flexibility of previous design, it was decided to avoid inheritance trees of \texttt{Task} subtypes, which would cause unnecessary complexity of database schema.

\begin{figure}[thb]
	\centering
	\includegraphics[width=\textwidth]{img/ConceptDiagram.eps}
	\caption{The concept diagram of the web application business layer.}
	\label{fig:diag:concept}
\end{figure}

Furthermore, the frontend classes include interface classes to core library: \texttt{LTSAlgorithm} and its subtypes an \texttt{LTSResult}. These classes serves to hide the implementation of core library and allow for extensibility of algorithms both in core library and in frontend application.

The storage of matrix data is handled by \texttt{Matrix} class, which serves as an interface for \texttt{Row} class. The \texttt{Row} class stores data inside of a database. Additionally, \texttt{Matrix} class is associated with multiple \texttt{ColumnTransformation} classes (for each column), which implements the functionality defined in requirement \ref{req:trans}.

\section{Core library processes}
\begin{figure}[htb]
	\centering
	\includegraphics[width=\textwidth]{img/SEQ_ComputeEstimateBSA.eps}
	\caption{Process of the BSA.}
	\label{fig:diag:coreproc:bsa}
\end{figure}

Core library consist of single main a process: computation of the OLS estimate. Depending on which algorithm is used, the flow of the process changes internally. The algorithms are implemented according to their pseudocode, separated into private methods for better code management.

Example flow for the BSA can be seen in Figure~\ref{fig:diag:coreproc:bsa}. First, actor creates a cache subtype and associates it with the algorithm solver. In the case of composite structure of BSA algorithm, instance of \texttt{LTSAlgorithmBSAM} is created internally and after method \texttt{solve()} is called, the BSA instance delegates call to the BSAM instance with appropriate \texttt{lowHParam} and \texttt{hParam}. During the computation, a \texttt{Combinations} instance is queried for each possible combination of data. This line corresponds to the line \ref{alg:bsa:combination} of the pseudocode Algorithm~\ref{alg:bsa}.

The inner loop abstracts lines \ref{alg:bsa:multih} to \ref{alg:bsa:multihend} and serves to show how solver instance interacts with the cache. This is accomplished by calling \texttt{checkCache} with vector as a key. The return value is the associated estimate or none; in that case a new OLS estimate is computed. After that follows the set of new optimum for a given $h$ if the currently found estimate is better than the best estimate so far.

The whole process ends when algorithm returns the evaluated estimate, associated vector and sum of residuals. In case of \texttt{LTSAlgorithmBSA} and \texttt{LTSAlgorithmVA} the solution is  a return value of \texttt{solve method}. In case of BSAM solver, the returned value is lowest optimum of for all requested $h$ parameters and user has to query for each result for given $h$ parameter by using additional method as illustrated in Figure~\ref{fig:diag:coreproc:bsam}.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.4\textwidth]{img/SEQ_GetResultBSAM.eps}
	\caption{Getting all results of BSAM.}
	\label{fig:diag:coreproc:bsam}
\end{figure}

\clearpage
\section{Frontend processes}
Business logic of frontend consists of three main processes: the queued computation, data input and data/result review. Both data input and review are mostly tied to web interface, while queued computation has no interaction with user except gathering initial data in data input phase. Supporting processes are canceling tasks and editing tasks after they have been created by the user.

\subsection{Queued computation}
Conceptual view of this process is depicted in Fig~\ref{fig:diag:webproc:queue}, where we can see the asynchronous computation of associated task data and subsequent saving of result. During the asynchronous computation the user can check the current status from the summary page. Design of the task state is done according to state design pattern and can be seen in Fig~\ref{fig:diag:webproc:state} and \ref{fig:diag:webproc:statetrans}.

\begin{figure}[htb]
	\centering
	\includegraphics[width=\textwidth]{img/ACT_QueueCompute.eps}
	\caption{Task queued processing.}
	\label{fig:diag:webproc:queue}
\end{figure}

\begin{figure}[htb]
	\centering
	\includegraphics[width=\textwidth]{img/ConceptDiagramState.eps}
	\caption{Task and state classes.}
	\label{fig:diag:webproc:state}
\end{figure}

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.8\textwidth]{img/ST_TaskState.eps}
	\caption{States transitions.}
	\label{fig:diag:webproc:statetrans}
\end{figure}

The computation is handled by the \texttt{Task} object by calling the method \texttt{compute} by one of the worker thread. The method first sets the appropriate state by calling method \texttt{doTransition} of current state. If the state determines the task is not in runnable state, the task will not run (for example, state cancelled will always cause the task to be skipped). If successful, task reads a relevant matrix and gives it to the associated algorithm to be computed. At the end, in case of success, status done is set. In case of failure, fail state is set instead. Sequence diagram of successful computation is visualized in figure \ref{fig:diag:webproc:taskcompute}. The computation process also includes setting of start and end time at the start and end of the computation process.

\begin{figure}[htb]
	\centering
	\includegraphics[width=\textwidth]{img/SEQ_TaskCompute.eps}
	\caption{Successful computation.}
	\label{fig:diag:webproc:taskcompute}
\end{figure}

\subsection{Data input}
Before task is computed, parameters and input data must be specified, this will be handled by UI forms. The form will be separated into multiple steps to provide simple step-by-step guide (wizard). The forms will also be able to check correctness of data before saving them to database. Sequence of forms and general flow of this process is illustrated in Figure~\ref{fig:diag:webproc:taskinput}. After the form is confirmed, the backend will send a mail to the user to confirm that task has been enqueued and link to the summary page.

\begin{figure}[htb]
	\centering
	\includegraphics[width=\textwidth]{img/ACT_DataInput.eps}
	\caption{Input data process.}
	\label{fig:diag:webproc:taskinput}
\end{figure}

\subsection{Result review}
By requirements, the summary page will be accessible to the user by the http link. To keep the minimum level of confidentiality, the link will include unique identifier created according to UUID version 1~\cite{RFC4122} linking directly to the TaskGroup object. There's a possibility of uuid being discovered and the link being accessed by a third party, but the length of uuid (128bits) makes the probability of finding a working link low.

The rendering of user view will be handled by associating the \texttt{TaskGroup} with \texttt{Renderer} type, which will translate the data from the model to the viewable form provided to the user. Using this method, we can simply create renderers for HTML output as well as for PDF or any other export.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.8\textwidth]{img/ConceptRenderer.eps}
	\caption{Renderer concept.}
	\label{fig:diag:webproc:summaryrender}
\end{figure}

\subsection{Cancel task}
Task canceling will be handled both by queue workers and by task themselves. In the case when the task is cancelled before it's run, the cancelling will be done by associating \texttt{StateCanceled} with the task. This will ensure that the task will be skipped as seen in Figure~\ref{fig:diag:webproc:queue}, \textit{task cancelled} branch.

\subsection{Edit task}
To ensure the consistency of the database and enqueued data, the task editing will be implemented as task duplication. This can easily be handled by duplicating appropriate model classes and editing them following the similar UI forms as in Input data process. The differences will be that the user will not be able to change the data matrix, because it's integral part of the task and this scenario can easily be fulfilled by creating a new task.

The forms will be pre-filled with data from the database, so that the user will only need to make changes he wants.

\newpage
\section{UI design}
Since the frontend application is web-based, the UI will be using standard technologies such as HTML, CSS and JavaScript. The application will consist of top-level pages to see algorithm overview, home pages, data input, short core library overview and summary page. The navigation chart can be seen in Figure~\ref{fig:diag:design:ui}. Note the one way link from summary page to index page, which makes summary page accessible only by following link given by task submitted page (or in sent mail) as requested by requirement~\ref{req:summarylink}.

\begin{figure}[htb]
	\centering
	\includegraphics[width=\textwidth]{img/Navigation.eps}
	\caption{navigation chart of the frontend view.}
	\label{fig:diag:design:ui}
\end{figure}