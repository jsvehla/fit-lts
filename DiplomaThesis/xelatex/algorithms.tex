﻿\chapter{Algorithms}\label{c:alg}
In this chapter we will describe algorithms used to compute the LTS estimate. The algorithms can be divided into two groups: exact and randomized algorithms.

Result of a probabilistic (randomized) algorithm is based on, as name implies, randomization; often used to speed up search by choosing only small portion of search space. This also implies that the optimal solution might not be found as it was removed from the search space. Advantage of these algorithms is that they are usually significantly faster compared to their counterpart---exact algorithms, which have to search the whole search space to ensure the optimal result in all runs.

\section{Fully random algorithms}
These algorithms can be compared to naive algorithm described in Section \ref{c:linreg:lts}. If we take the most robust value of $h$ (computed as in \eqref{eq:linreg:h}), the number of sets that need to be searched is approximately $\binom{n}{\frac{n}{2}}$. One way to lower this number is to limit the number of combinations used to get the result. For example, probabilistic algorithms use randomization to select limited amount from all combinations of measurements in data. The set of all combination is denoted as set $Q$ and defined according to~\cite{klouda07} in Section 2.2.

One such algorithms is called \textit{Random solution algorithm}~\cite{WeiBai03} (Algorithm~ \ref{alg:rsa}). The size of searched subset $L$ is dependent on user-provided values $\epsilon$ (error bound) and $\delta$ (confidence interval).

In pseudocode we define new symbols $w$ and function $J$. Symbol $w$ denotes a vector of not-trimmed data points for the estimate. It can be coded as either binary vector of size $n$ where count of `1' (considering 1 signifies a presence of point and 0 its absence) is $h$ or as a vector of rows of size $h$. Function $J$ is the minimum of the objective function.

\begin{algorithm}[htb]
\caption{Random solution algorithm.}
\label{alg:rsa}
\DontPrintSemicolon
\SetKwInOut{Input}{I}\SetKwInOut{Output}{O}
\Input{Error bound $\epsilon$, confidence interval $\delta$, explanatory variables X, response variable Y, parameter h}
\Output{LTS estimate}
\BlankLine
Choose integer $L$ so that $L = \lceil\frac{1-\epsilon}{\delta}\rceil$\;
$J_{min} \leftarrow\infty$\;
\For{$IterCnt\leftarrow 0$ \KwTo $L$}{
	$w \leftarrow$ random vector from $Q^{(n,h)}$\;
	\If{$J_{min} > J(w)$}{
		$J_{min}\leftarrow J(w)$\;
		$w_{min}\leftarrow w$\;
	}
}
\Return $\hat{\beta}^{\left(OLS, W_{min}X, W_{min}Y\right)}$ as LTS estimate, where $W_{min}$ is a $diag(w_{min})$\;
\end{algorithm}

\section{Weak necessary condition}

Weak necessary condition algorithm~\cite{Visek96} (also called Víšek algorithm) is probabilistic algorithm that uses the randomization to choose a starting point for initial step of each cycle. After starting point is chosen, algorithm searches for points fulfilling weak necessary condition. The algorithm is guaranteed to find the exact solution only if ``good'' starting point is chosen. This algorithm belongs to the group of \textit{Monte Carlo} randomized algorithms and simple pseudocode can be seen in Figure~\ref{alg:va}.

Pseudocode define additional terms; the term $w$-trimmed means points that are excluded from the OLS estimate, while $w$-not-trimmed denotes points included. For example, for the binary vector $w = (0, 1, 0, 1, 1)$, $w$-not-trimmed points are 2, 4 and 5, while $w$-trimmed points are 1 and 3. Matrix $W$ in equation is created as $W = diag(w)$ and serves to eliminate trimmed points from the estimate.

\textbf{Weak necessary condition:} Vector \textit{w} satisfies \textit{weak necessary condition} if and only if $r^{2}\left(\hat{\beta}^{\left(OLS,WX,WY\right)}\right)$ for each w-trimmed is greater or same that each w-not-trimmed point.


\begin{algorithm}
\caption{Weak necessary condition algorithm.}
\label{alg:va}
\DontPrintSemicolon
\SetKw{Goto}{repeat from}
\SetKwInOut{Input}{I}\SetKwInOut{Output}{O}
\Input{Maximum number of iterations $Iter_{max}$, matrix of explanatory variables X, vector of response variable Y, parameter h}
\Output{LTS estimate}
\BlankLine
$IterCnt \leftarrow 0 $\;
$J_{min} \leftarrow \infty $\;
\While{$IterCnt \leq Iter_{max}$}{
	$w \leftarrow$ random vector of h w-not-trimmed points\;
	Compute OLS estimate for w-not-trimmed points\;\label{alg:va:goto01}
	Get residual $r^{2}_{i}(\hat{\beta}^{(OLS,WX,WY)})$ for all points\;
	Sort tem and choose vector $\tilde{w}$ as vector of  points with h lowest residuals\;
	\If{$J(\tilde{w}) < J(w)$}{
		$w \leftarrow \tilde{w}$\;
		\Goto \ref{alg:va:goto01}\;
	}
	\If{$J(w) < J_{min}$}{
		$J_{min}\leftarrow J(w)$\;
		$\hat{\beta}^{\left(VA,n,h\right)} \leftarrow\hat{\beta}^{\left(OLS,WX,WY\right)}$\;
	}
	$IterCnt\leftarrow IterCnt + 1$\;
}
\Return $\hat{\beta}^{\left(VA,n,h\right)}$ as LTS estimate
\end{algorithm}

\section{Borders scanning algorithm}\label{c:alg:bsa}
Exact algorithms guarantee finding an optimal solution. In this section we will introduce an algorithm called \textit{Border scanning algorithm} (BSA)~\cite{klouda07}.

The algorithm searches for points in set $\mathcal{H}$ which are then used to compute the LTS estimate. The set $\mathcal{H}$ is defined in Section 2.2.1 of~\cite{klouda07} and for every $h \in \mathcal{H}$ the condition
\begin{equation}\label{eq:hequal}
	r^{2}_{\left(h\right)}\left(h_{n}\right) = r^{2}_{\left(h+1\right)}\left(h_{n}\right)
\end{equation}
holds true. The algorithm correctness proof also shows that the points are also intersection points of OLS objective functions, which LTS estimate consists of.

Let's use a simple example with one explanatory variable (parameter p is 2) to explain the algorithm. In each intersection point $h_{n}$, we can say that $h$ lowest residuals are equal as seen in \eqref{eq:hequal}. Solving such system is difficult, because the equation includes ordering of residuals by their value. We can overcome this difficulty by solving system:
\begin{equation}\label{eq:hsystem}
	r^{2}_{i}\left(\beta\right) = r^{2}_{j}\left(\beta\right),  i,j \in \left\{1, 2, \dots, n \right\}, i \neq j.
\end{equation}

For each pair i, j we will get two points $\beta_{1}$ and $\beta_{2}$ where $i$-th and $j$-th residuals equal. By computing and ordering residuals in these points, we get two sorted vectors. If the condition \eqref{eq:hequal} holds, we found a point where two objective functions of the OLS estimates intersect. This can be simply described if we realize that for different values of $\beta$, residuals change their relative position. These changes do not effect the objective function, but it can be proven that in the intersection $h$-th and $(h+1)$-th residual change positions.

The same methods is applied to both ordered vectors. In short, we can gradually find all $\beta$ fulfilling the condition \eqref{eq:hequal} (the intersections). The algorithm then continues by computing OLS estimate for two intersecting functions for each intersection and if the estimate is lower that current optimum, the algorithm remembers it. By finding all intersections we can assume we found the LTS estimate.

This method can be applied to multidimensional case ($p \geq 2$) by solving system
\begin{align}\label{eq:multisystem}
r^{2}_{i_{1}}\left(\beta\right) 	&=		r^{2}_{i_{2}}\left(\beta\right)		\notag\\
									&\mathrel{\makebox[\widthof{=}]{\vdots}}	\\
r^{2}_{i_{p}}\left(\beta\right) 	&=		r^{2}_{i_{p+1}}\left(\beta\right)	\notag
\end{align}
for all distinct $i_1\dots i_{p+1} \in \left(1, \dots, n\right)$ instead of \eqref{eq:hsystem}.

Significant difference, that needs to be taken into consideration, is that more that one function intersect in intersection points. The proof of the correctness of the algorithm can again be found in~\cite{klouda07} and the algorithm pseudocode is shown as Algorithm~\ref{alg:bsa}

Since the system \eqref{eq:multisystem} in this form cannot be easily solved, the algorithm uses an alternative solution by transforming it into multiple systems of linear equations using symbol $\circ$ to denote the minus or plus sign (see line \ref{alg:bsa:eqsystem}). For example, cycle $l = 10011_{\left(2\right)}$ would mean solving system with signs in order $\left\{+, -, -, +, +\right\}$.

The algorithm allows computing multiple values of parameter h at the same time with lower overhead that computing them separately. This can be accomplished by inserting \texttt{for} cycle at the line \ref{alg:bsa:multih} ending at line \ref{alg:bsa:multihend}. This is allowed because finding intersection points is independent of parameter h and the parameter h is first used on the line \ref{alg:bsa:multih}, where algorithm decides if the found point is or isn't intersecting point for given h. It's obvious that by comparing with multiple values of h we can find associated intersections without recomputing previous system. This modification is referred to as \textit{BSA extended} or, in short, BSAM (as in BSA multiple).

Algorithm can be optimized even further by disallowing computations for one objective function multiple times. The multiple computations occurs because each function intersect with multiple functions and the described algorithm doesn't take this into consideration.

\begin{algorithm}[tb]
\caption{Borders scanning algorithm.}
\label{alg:bsa}
\DontPrintSemicolon
%\SetKw{Repeat}{repeat from}
\SetKw{Continue}{continue}
\SetKw{Goto}{goto}
\SetKwInOut{Input}{I}\SetKwInOut{Output}{O}
\Input{Matrix of explanatory variables X, vector of response variable Y, parameter h}
\Output{LTS Estimate}
\BlankLine
$J_{min} \leftarrow \infty $\;
$w_{min} \leftarrow nul $\;
\ForEach{$v_{k} \in Q^{\left(n, p+1 \right)}$}{\label{alg:bsa:combination}
	Create a vector $(i_1, dots, i_{p+1}$ as a vector of indices of not-trimmed points in subset $v_k$ \;
	\For{$l \leftarrow 1$ \KwTo $2^p$}{
		If the system of equations:
		\begin{align}
			\left(x_{i_{1}} \circ_{1} x_{i_{2}}\right)\beta 	&= y_{i_{1}} \circ_{1} y_{i_{2}}			\notag\\
																&\mathrel{\makebox[\widthof{=}]{\vdots}} 	\\
			\left(x_{i_{1}} \circ_{p} x_{i_{p+1}}\right)\beta 	&= y_{i_{1}} \circ_{p} y_{i_{p+1}}			\notag
		\end{align}
		is regular, denote its solution $\beta_{0}$, else \Continue\;\label{alg:bsa:eqsystem}
		Evaluate and order residuals at $\beta_{0}$\;
		\If{$r^{2}_{i_{1}}\left(\beta_{0}\right) = r^{2}_{\left(h\right)}\left(\beta_{0}\right) = r^{2}_{\left(h + 1\right)}\left(\beta_{0}\right)$}{\label{alg:bsa:multih}
			$cPart \leftarrow$ vector of data points having residuals $< r^{2}_{i_{1}}\left(\beta_{0}\right)$\;
			$hPart \leftarrow$ vector of data points having residuals $= r^{2}_{i_{1}}\left(\beta_{0}\right)$\;
			\ForEach{$comb = \left(h - length\left(hPart\right)\right)$ combination of $hPart$}{
				$w \leftarrow$ concat$\left(cPart, comb\right)$\;
				\If{$J\left(w\right) \leq J_{min}$}{
					$J_{min} \leftarrow J\left(w\right)$\;
					$w_{min} \leftarrow w$\;				
				}
			}
		}\label{alg:bsa:multihend}
	}	
}
\Return $\hat{\beta}^{\left(OLS,W_{min}X, W_{min}Y\right)}$, $W_{min}s = diag\left(w_{min}\right)$ as LTS estimate
\end{algorithm}
