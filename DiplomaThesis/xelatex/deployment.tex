\chapter{Deployment and testing}\label{c:deployment}
In this chapter, we will describe how to install required libraries for both parts of the application to work using provided build scripts and other internal tools. Furthermore, we will mention testing methods and individual test to ensure the application is working correctly.

Main target platform for deployment will be Linux. The application was tested on LUbuntu Linux and the disk image of development environment is provided on enclosed DVD.

\section{Git repositories}
To allow easy development and remote backup of the project, the git versioning system was used. The remote project repository was hosted at \url{https://bitbucket.org/} and separated into C++ library repository and Django application repository. The adresses of the projects are:
\begin{description}\itemsep1pt \parskip0pt \parsep0pt
	\item[FIT-LTS]\hfill\\ 			\url{https://bitbucket.org/jsvehla/fit-lts}
	\item[FITLTS-Django]\hfill\\	\url{https://bitbucket.org/jsvehla/fitlts-django}
\end{description}

Direct links to the git repositories are:
\begin{description}\itemsep1pt \parskip0pt \parsep0pt
	\item[FIT-LTS]\hfill\\			\url{https://bitbucket.org/jsvehla/fit-lts.git}
	\item[FITLTS-Django]\hfill\\ 	\url{https://bitbucket.org/jsvehla/fitlts-django.git}
\end{description}

Projects were set to public read access and are freely available to be downloaded. To checkout using git, the user needs to run appropriate git commands (e.g. clone) with his BitBucket account and password.

\section{C++ library}
C++ library can be easily deployed on Windows and Linux.

First checkout the library code located at \url{https://bitbucket.org/jsvehla/fit-lts/overview} or copy code from enclosed DVD.

To compile with the VS2010 project files located inside the folder \texttt{VS2010} user only needs to install Boost libraries and point the Additional Includes in project properties paths to the installation folder. VS project files use relative paths to locate needed libraries inside the folder \texttt{lib} in the project root folder. The project files assume that boost is installed inside folder \texttt{C:\textbackslash libs\textbackslash boost\_1\_53\_0}. 

\vspace{0.5em}
\noindent The folder \texttt{lib} should look like this:
\dirtree{%
	.1 lib				\DTcomment{library folder}.
	.2 armadillo-3.4.4	\DTcomment{Armadillo library}.
	.2 armanpy-0.1.1	\DTcomment{Armanpy library}.
	.2 googletest		\DTcomment{Googletest library}.
	.2 tclap			\DTcomment{TCLAP library}.
}\vspace{0.5em}

After the correct paths are set, user can simply compile CLI, object library and testing suite with MSVC compiler. Output folder for the MSVC project is set to \texttt{bin}.

As the alternative option or to deploy on Linux, the project includes CMake build scripts. First script, located directly inside the root folder, builds the LTS library. The second one, inside folder \texttt{src\textbackslash py\_wrapper} serves to build the wrapper. The build scripts are written to be easily understood. For details how to set the paths to the required libraries, refer to the commented build scripts.

The building scripts have paths currently set accordingly Linux folder and libraries structure. To compile the LTS library, user has to install Armadillo, LAPACK, BLAS, TCLAP and Boost either by using package management systems or directly by following installation guides.

After the compile environment is set, CMake can easily compile the library using these commands run in the root folder (to compile library) or in the \texttt{src\textbackslash py\_wrapper} (to compile wrapper)
\begin{Verbatim}
> mkdir build
> cd build            # create build folder
> cmake ..            # generate gcc/msvc files
> cmake --build .     # compile
\end{Verbatim}
Resulting files are then placed inside the \texttt{build} folder.

\section{Django application}
This section deals with the required settings and processes to make web application run. To show this we use Django internal webserver, which is only recommended for development server. To use application in production environment, the HTTP requests needs to be handled by HTTP server such as Apache with appropriate settings. How to set the webserver is outside of the scope of this thesis, but basics and recommendations can be found in the Django documentation~\cite{DjangoWSGI}.

In the following sections is shows how to install the required tools together with links to the official documentations. While installing packages from repositories is easy, the recommended way is to read and follow linked documentations.

\subsection{Required modules and libraries}
Django can be installed by following the page Installing Django in the official documentation~\cite{DjangoInstall}. The needed steps are installation of Python, Django and database engine. Using Ubuntu repositores, the simplest way to do this is to run the following commands:
\begin{Verbatim}
> sudo apt-get install python2.7
> sudo apt-get install python-django
> sudo apt-get install sqlite3
\end{Verbatim}

Afterwards, NumPy libraries needs to be installed either from source or using Linux package manager. Installation instructions are to be found in the official documentation~\cite{NumPyInstall}. 
Again, the package can be installed directly from repositories.
\begin{Verbatim}
> sudo apt-get install python-numpy
\end{Verbatim}

As mentioned in the implementation chapter, the application uses Celery as a queue manager. Celery needs a broker to work and need to be installed. Simple steps how to install Celery and RabbitMQ can be found in Celery documentation under the label "First Steps"~\cite{CeleryFirstSteps} and "Django"~\cite{CeleryDjango}. To allow sending mails the templated-email library also needs to be installed. 

Short overview of instalation commands:
\begin{Verbatim}
> sudo apt-get install rabbitmq-server
> pip install celery
> pip install django-celery
> pip install django-templated-email
\end{Verbatim}

The rabbitmq package should handle all required setting to the Linux system to run the RabbitMQ server at boot. Pip is an Python package manager. In case the tool didn't come with the Python distribution, it can be easily installed from repository under the name \texttt{python-pip}.

To allow export to PDF, the XHTML2PDF library\footnote{https://pypi.python.org/pypi/xhtml2pdf} and its requirements needs to be installed. To install using pip run these commands:
\begin{Verbatim}
> pip install xhtml2pdf
> pip install reportlab
> pip install html5lib
\end{Verbatim}

\subsection{Settings}
Application settings module is edited to use relative paths to required folders and modules (for example database path or template folders). The comprehensive list of Django settings and their effects can be found in Django documentation~\cite{DjangoSettings}, for Celery specific settings look into Celery documentation~\cite{CelerySettings}. All relative paths are evaluated from the root of the project folder using \texttt{SITE\_ROOT} as a base.

\noindent The specific or not default settings are as follows:
\begin{itemize}\itemsep1pt \parskip0pt \parsep0pt
\item database.py
\begin{description}
	\item[DATABASES]\hfill \\		Settings for the database engine. Set to the SQLite database file located at \texttt{SQLiteDB/sqlite3.db}.
\end{description}
\item settings.py
\begin{description}
	\item[SITE\_ROOT]\hfill \\ 			Path to the root of the project folder. Retrieved  and set when the settings are loaded.
	\item[STATICFILES\_DIRS]\hfill \\ 	Path to the folder with static files. Points to the \texttt{static/}
	\item[TEMPLATE\_DIRS]\hfill \\ 		Paths to the folders with template files for each subproject. Set to the \texttt{LTSAlgorithms/templates/} and \texttt{FITLTS/templates/}
	\item[INSTALLED\_APPS]\hfill \\ 	Names of the modules (subprojects) loaded into application. Includes all subprojects and djcelery module.
\end{description}
\item celery.py
\begin{description}
	\item[BROKER\_URL]\hfill \\						Setting for celery backend.
	\item[CELERY\_RESULT\_BACKEND]\hfill \\			Settings for result backend to allow handling of results and storing them.
	\item[CELERY\_TASK\_RESULT\_EXPIRES]\hfill \\	Time until stored result is deleted. Set to None - never.
	\item[TEST\_RUNNER]\hfill \\ 					Settings for test framework to allow testing celery tasks. 
	\item[CELERY\_IMPORTS]\hfill \\					List of modules which includes celery tasks. Point to the \texttt{Queue\-Celery.\-models.\-queue\-tasks} where the compute task is located.
\end{description}
\item application.py
\begin{description}
	\item[MAX\_ALGORITHMS\_PER\_GROUP]\hfill \\		Maximum number of allowed tasks per group. This settings affect input forms and not database constraints.
	\item[TRANSFORMATION\_CLS]\hfill \\				List of classes for matrix transformations. Use full module names.
	\item[ALGORITHM\_CLS]\hfill \\					List of tuples (algorithm, algorithm form) for registered algorithms. Each algorithm has associated form. Use full module names.
\end{description}
\item mail.py
\begin{description}
	\item[EMAIL\_HOST]\hfill \\					STMP host server. Currently set to use gmail stmp with one-purpose account instead of standalone mail server.
	\item[EMAIL\_PORT]
	\item[SERVER\_EMAIL]
	\item[DEFAULT\_FROM\_EMAIL]\hfill \\		Default sender address
	\item[EMAIL\_USE\_TLS]
	\item[EMAIL\_HOST\_USER]\hfill \\			Google Username. Currently set to \textit{fitlts.django@gmail.com}
	\item[EMAIL\_HOST\_PASSWORD]\hfill \\		Google Password. Currently set to \textit{djangolts13}
	\item[TEMPLATED\_EMAIL\_BACKEND]\hfill \\	Settings for templated-email library backend. Set to vanilla django backed.
\end{description}
\end{itemize}

\subsection{Starting the application}
To prepare django application for run, run the following commands in the root folder of django application:
\begin{Verbatim}
> python manage.py syncdb
\end{Verbatim}
The command will create tables in the database. This command is simple and cannot edit already existing tables, only add new. It's recommended to clear the database before the first run. If default settings are used this can be done by simply removing SQLite database file and running the above command.

\begin{Verbatim}
> python manage.py runserver
> python manage.py celery
\end{Verbatim}
First command will start the built-in webserver and the application will then start accepting requests at \url{127.0.0.1:8000} with index available at \url{127.0.0.1:8000/index} and access to the administration interface at \url{127.0.0.1:8000/admin}. Second command (run in other window) will start executing celery queue.


\section{Testing}
Both core library and frontend application include basic test cases. The core library uses Googletest library\footnote{\url{http://code.google.com/p/googletest/}} for the tests. The test cases must be compiled before they can be run. This can be done on Windows by simply running compile in Visual Studion project. The test cases for the core library consist of computing LTS estimates of datasets in \texttt{test/data} using all implemented algorithms. For more info about these datasets see appendix.

The frontend application uses internal Django testing framework. Each sub-project includes a folder \texttt{tests} with testcases to test functionality provided by the sub-project classes. The tests combine basic unit testing and more complex tests. For more details about each test reffer to the appropriate test file.

The tests in Django can be run using the command:
\begin{Verbatim}
> python manage.py test
\end{Verbatim}
which will then run all test including internal Django tests to chec correct instalation. Django allows to specify sub-project or even test method. For example:
\begin{Verbatim}
> python manage.py test LTSTasks.ComputeTests.testComputeVA
\end{Verbatim}

\section{VM image}
To provide easy way to tryout the application without the neeed of configuring the environment, the enclosed DVD includes the image of development system for Oracle Virtual box\footnote{\url{https://www.virtualbox.org/}}. The image was exported using VB version 4.2.12 and can be easily imported by using import appliace function in File menu.

The image uses Lubuntu OS. Used username is \textit{user} and password is the same, \textit{user}. The application can be found in directory \texttt{/home/\allowbreak user/\allowbreak desktop/\allowbreak FIT-LTS}.

To update the local copy to the up-to-date version in git repository, the git can be used:
\begin{Verbatim}
> git pull
\end{Verbatim}
Git settings included in the .git folder will be used to download the current version from the repository, user only needs to provide the username and password of his BitBucket account.

