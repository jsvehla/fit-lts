\chapter{Linear regression}\label{c:linreg}

\section{Regression analysis}
Regression analysis is a field of statistics. The aim of the regression methods is to find and examine a relation between several explanatory variables and a response variable. For linear regression this relation is expected to be linear and satisfying the linear regression model
\begin{equation}
Y_{i} = x_{i}^{T} \beta^{0} + e_{i}, \; i \in \left\{1, \dots n\right\}
\end{equation}
where $Y$ denotes the response variable, $x$ is a vector of explanatory variables, $\beta \in \mathbb{R}^{p}$ is an unknown vector of regression coefficients, $e$ is an error term and $n$ is the number of data points.

One of the most popular methods for estimating regression coefficients is the least squares method. We will consider two versions: the ordinary least squares (OLS) and least trimmed squares (LTS). The difference between these versions is that the OLS is not robust.

As an example of linear regression we will use the clinical analysis of a relations between the height of a child and its father. For the example data we have one explanatory variable $p$ (father's height) and one response variable $ch$ (child's height in adulthood). By solving the system
\begin{equation}
ch_i = p_i\beta^{1} + \beta^{0}, \; i \in \left\{1, \dots n\right\}
\end{equation}
we can describe the relation by vector of coefficients $\beta = \left(\beta^{0}, \beta^{1}\right)^{T}$. Using this coefficients in equation, where response variable value is unknown, we will get the estimated value of child's size in adulthood. Simple example graph showing the linear regression can be seen in the Fig. \ref{fig:lin-reg}.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.85\textwidth]{img/lin-reg01.eps}
	\caption{Linear regression of parent-child height relation.}	
	\label{fig:lin-reg}
\end{figure}

\clearpage
\begin{samepage}
\noindent For illustration of linear regression we use R statistical software. In the following examples we use randomly generated heights in cm (vectors $p$ and $ch$) to calculate a result $fit$ using linear model function from R. The result is an R object encapsulating various properties of fit function and can be queried for summary by using \texttt{summary} function.
\begin{Verbatim}[xleftmargin=1em]
> p  <- c(165, 175, 180, 179, 170, 168, 179, 176)
> ch <- c(164, 174, 178, 182, 169, 171, 176, 174)
> fit <- lm(ch ~ p)
> summary(fit)
Call:
lm(formula = ch ~ p)
Residuals:
Min      1Q  Median      3Q     Max
-2.0089 -1.3237 -0.9018  0.4263  3.9911

Coefficients:
	Estimate Std. Error t value Pr(>|t|)
(Intercept)  16.5893    27.6224   0.601  0.57010
p             0.9018     0.1587   5.683  0.00128 **
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 2.375 on 6 degrees of freedom
Multiple R-squared: 0.8433,     Adjusted R-squared: 0.8172
F-statistic:  32.3 on 1 and 6 DF,  p-value: 0.00128
\end{Verbatim}
\end{samepage}

By using these commands, the resulting fit function is $ch = 0.9018p + 16.5893$.

In reality, children height is dependent on more than one factor. To include these dependencies in the model, we'd need to use more explanatory variables.

\section{Ordinary least squares}
We have said this method is not robust. Robustness of the regression method can be described as the rate of how much can the estimate be influenced by changes in data. Using our previous example, let's imagine that one of the fathers suffer from a non-hereditary disorder and his height did not influence child's height in the expected way. Including this data point in the model can significantly influence the result. Points that influence the result in unwanted way are usually called \textit{outliers}.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.85\textwidth]{img/lin-reg02.eps}
	\caption{Linear regression of parent-child height relation with outlier data (black).}
	\label{fig:lin-reg-outlier}
\end{figure}

\clearpage
\begin{samepage}
Adding the example outlier point (161, 184) to our dataset and using the same commands, we get:
\begin{Verbatim}[xleftmargin=1em]
> summary(fit)
Call:
lm(formula = ch ~ p)
Residuals:
Min     1Q Median     3Q    Max
-9.038 -2.685 -1.194  1.729 11.824

Coefficients:
	Estimate Std. Error t value Pr(>|t|)
(Intercept) 137.4709    58.1048   2.366   0.0499 *
p             0.2156     0.3365   0.641   0.5422
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 6.509 on 7 degrees of freedom
Multiple R-squared: 0.05538,    Adjusted R-squared: -0.07957
F-statistic: 0.4104 on 1 and 7 DF,  p-value: 0.5422
\end{Verbatim}
\end{samepage}

As we can see, the resulting estimate is now $ch = 0.2156p + 137.4709$. The figure \ref{fig:lin-reg-outlier} uses this example outlier to show how even a single outlier can have significant impact on the result.

The similar situation can arise if we, unknowingly, work with multiple datasets mixed into one. In that case, the result is influenced by both datasets and is not correct for either one. Simple solution would be to separate these datasets, but without knowing about them being mixed or how we might not be able to.

Formally, the estimate given by the OLS method is defined as the minimum of sum of squared residuals. The residual for the measurement $i$ and $\beta$ is given by
\begin{equation}
r_{i}\left(\beta\right) = Y_{i} - x_{i}^{T}\beta
\end{equation}
and the OLS estimate is the vector of regression coefficients satisfying
\begin{equation}
\hat{\beta}^{\left(OLS,n\right)} = \argmin_{\beta\in R^{p}}\sum_{i=1}^{n} r^{2}_{i} \left(\beta\right) = \argmin_{\beta\in R^{p}}\left(Y - X \beta\right)^{T}\left(Y - X \beta\right)
\end{equation}
where $X \in \mathbb{R}^{n,p}$ is the matrix of explanatory variables ($x_i$ is $i$-th row of $x$) and $Y \in \mathbb{R}^{n}$ is the vector of response variable.

The equations can be simplified as
\begin{equation}\label{eq:linreq:j}
\hat{\beta}^{\left(OLS,n\right)} = \left(X^{T}X\right)^{-1}X^{T}Y.
\end{equation}

\section{Least trimmed squares}\label{c:linreg:lts}
Compared to the OLS, the least trimmed squares method is not so sensitive to outliers. This is achieved by removing points that are outside of the main bulk of the data. These points are usually called outliers. For example, by removing a black outlier in Figure~\ref{fig:lin-reg-outlier} and computing the OLS estimate only for the remaining points. Points removed this way are called the \textit{trimmed points}. The remaining points are, by the same rule, called \textit{not-trimmed} and we denote the number of not-trimmed points as a parameter \textit{h}.

The LTS method is mathematically defined as the minimum of the objective function of the LTS. Following the same denotation as~\cite{klouda07} where $r^{2}_{\left(i\right)} \left(\beta\right)$ means $i$-th squared residual in ordered set of residuals, the objective function of LTS can be defined as:
\begin{equation}
\hat{\beta}^{OLS,n,h} = \argmin_{\beta\in R^{p}}\sum_{i=1}^{h} r^{2}_{\left(i\right)} \left(\beta\right).
\end{equation}

A naive algorithm for finding the LTS estimate could be described as $\binom{n}{h}$ runs of the OLS algorithm, such that in every turn we compute OLS for different set of $h$ not-trimmed points. The LTS result would then be the smallest sum of residuals of all computed OLS estimates. Unfortunately, even with a small number of data, the search space grows exponentially to the point where search becomes computationally difficult or impossible.

The parameter $h$ can be set to any value between 1 and $n$, but the most ideal value for $h$ would be a number of points without outliers. Since finding outliers in advance is difficult, parameter $h$ is usually set to maximize \textit{breakdown point} defined and explained in~\cite{Leroy87}. The most robust value for the breakdown point is 0.5 and is achieved for:
\begin{equation}\label{eq:linreg:h}
h = \left[\frac{n}{2}\right] + \left[\frac{p+1}{2}\right],
\end{equation}
where $p$ is a number of explanatory variables.
